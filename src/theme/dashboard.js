import React from 'react';
import {connect} from "react-redux";
import { Link } from "react-router-dom";

import Sidemenu from './sidemenu/sidemenu';
import PageContent from './pagecontent/pagecontent';
import TopNavigation from './topnavigation/topnavigation';

// DEFAULT SETTINGS ON AXIOS DASHBOARD PAGE
import axios from 'axios';
axios.defaults.headers.get['Content-Type'] = 'application/json';
axios.defaults.headers.get['Access-Control-Allow-Origin'] = '*';

axios.defaults.headers.post['Content-Type'] = 'application/json';
axios.defaults.headers.post['Access-Control-Allow-Origin'] = '*';

axios.defaults.headers.put['Content-Type'] = 'application/json';
axios.defaults.headers.put['Access-Control-Allow-Origin'] = '*';


function Dashboard(props) {

  React.useEffect(()=>{
    document.body.style.backgroundColor = "#2A3F54";

    axios.defaults.headers.get['tracker-dash-token'] = props.state.auth.data["tracker-dash-token"];
    axios.defaults.headers.post['tracker-dash-token'] = props.state.auth.data["tracker-dash-token"];
    axios.defaults.headers.put['tracker-dash-token'] = props.state.auth.data["tracker-dash-token"];
  });

  return(
    <div className="nav-md body">
      <div className="container body">
        <div className="main_container">
          <div className="col-md-3 left_col">
            <div className="left_col scroll-view">
              <div className="navbar nav_title" style={{Border: '0'}}>
                <Link to="" className="site_title"><i className="fa fa-paw"></i> <span>GoRockyGo</span></Link>
              </div>
              <div className="clearfix"></div>
              {/* <!-- menu profile quick info --> */}
              <div className="profile clearfix">
                <div className="profile_pic">
                  {/* <img src="#" alt="..." className="img-circle profile_img"/> */}
                </div>
                <div className="profile_info">
                  <span>Welcome, {props.state.auth.data.userName}</span>
                  {/* <h2>{props.state.auth.data.userName}</h2> */}
                </div>
              </div>
              {/* <!-- /menu profile quick info --> */}
              <br />
              
              <Sidemenu/>
              
              {/* <!-- /menu footer buttons --> */}
              <div className="sidebar-footer hidden-small hide">
                <Link to="" data-toggle="tooltip" data-placement="top" title="Settings">
                  <span className="glyphicon glyphicon-cog" aria-hidden="true"></span>
                </Link>
                <Link to="" data-toggle="tooltip" data-placement="top" title="FullScreen">
                  <span className="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
                </Link>
                <Link to="" data-toggle="tooltip" data-placement="top" title="Lock">
                  <span className="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
                </Link>
                <Link to="" data-toggle="tooltip" data-placement="top" title="Logout" href="login.html">
                  <span className="glyphicon glyphicon-off" aria-hidden="true"></span>
                </Link>
              </div>
              {/* <!-- /menu footer buttons --> */}

            </div>
          </div>

          {/* Top Navigation */}
          <TopNavigation/>

          {/* PAGE CONTENT */}
          <PageContent/>
        </div>
      </div>
    </div>
  );
  
}


const mapStateToProps = (state) => {
  return {
    state: state
  }
}

const mapDispatchProps = (dispatch) => {
  return {
    
  }
}

export default connect(mapStateToProps,mapDispatchProps)(Dashboard);