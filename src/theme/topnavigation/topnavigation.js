import React from 'react';
import { Link } from "react-router-dom";
import {connect} from "react-redux";


function TopNavigation(props) {
    return(
        <div className="top_nav">
            <div className="nav_menu">
                <nav>
                    <div className="nav toggle">
                    <Link to=""  id="menu_toggle"><i className="fa fa-bars"></i></Link>
                    </div>
                    <ul className="nav navbar-nav navbar-right">
                    <li>
                        <Link to="" className="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                           {props.state.auth.data.userName} <span className=" fa fa-angle-down"></span>
                        </Link>
                        
                        <ul className="dropdown-menu dropdown-usermenu pull-right">
                            <li><Link to=""> Profile</Link></li>
                            {/* <li>
                                <Link to="">
                                <span className="badge bg-red pull-right">50%</span>
                                <span>Settings</span>
                                </Link>
                            </li>
                            <li><Link to="">Help</Link></li> */}
                            <li><Link to="" onClick={props.setLogout}><i className="fa fa-sign-out pull-right"></i> Log Out</Link></li>
                        </ul>
                    </li>

                    {/* <li role="presentation" className="dropdown">
                        <Link to="" className="dropdown-toggle info-number" data-toggle="dropdown" aria-expanded="false">
                            <i className="fa fa-envelope-o"></i>
                            <span className="badge bg-green">6</span>
                        </Link>
                        <ul id="menu1" className="dropdown-menu list-unstyled msg_list" role="menu">
                            <li>
                                <Link to="">
                                    <span className="image">
                                        <img src="images/img.jpg" alt="Profile Image1" />
                                    </span>
                                    <span>
                                        <span>John Smith</span>
                                        <span className="time">3 mins ago</span>
                                    </span>
                                    <span className="message">
                                        Film festivals used to be do-or-die moments for movie makers. They were where...
                                    </span>
                                </Link>
                            </li>
                            <li>
                                <Link to="">
                                    <span className="image">
                                        <img src="images/img.jpg" alt="Profile Image2" />
                                    </span>
                                    <span>
                                        <span>John Smith</span>
                                        <span className="time">3 mins ago</span>
                                    </span>
                                    <span className="message">
                                        Film festivals used to be do-or-die moments for movie makers. They were where...
                                    </span>
                                </Link>
                            </li>
                            <li>
                                <Link to="">
                                    <span className="image">
                                        <img src="images/img.jpg" alt="Profile Image3" />
                                    </span>
                                    <span>
                                        <span>John Smith</span>
                                        <span className="time">3 mins ago</span>
                                    </span>
                                    <span className="message">
                                        Film festivals used to be do-or-die moments for movie makers. They were where...
                                    </span>
                                </Link>
                            </li>
                            <li>
                                <Link to="">
                                    <span className="image"><img src="images/img.jpg" alt="Profile Image4" /></span>
                                    <span>
                                        <span>John Smith</span>
                                        <span className="time">3 mins ago</span>
                                    </span>
                                    <span className="message">
                                        Film festivals used to be do-or-die moments for movie makers. They were where...
                                    </span>
                                </Link>
                            </li>
                            <li>
                                <div className="text-center">
                                    <Link to="">
                                        <strong>See All Alerts</strong>
                                        <i className="fa fa-angle-right"></i>
                                    </Link>
                                </div>
                            </li>
                        </ul>
                    </li> */}
                    </ul>
                </nav>
            </div>
        </div>
    )
}

const mapStatetoProps = (state) => {
    return {
        state: state
    }
}
const mapDispatchToProps = (dispatch) => {
    return {
        setLogout:(payload) => dispatch({type:'AUTH_LOGOUT',payload:payload})  
    }
}
export default connect(mapStatetoProps,mapDispatchToProps)(TopNavigation);