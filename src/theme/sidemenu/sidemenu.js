import React from 'react';
import { NavLink, withRouter , Link} from "react-router-dom";

function Sidemenu() {


	const getNavLinkClass = (path) => {
		return window.location.pathname === path ? 'current-page' : '';
	}

	return (
		<div id="sidebar-menu" className="main_menu_side hidden-print main_menu">
			<div className="menu_section">
				<h3>General</h3>
				<ul className="nav side-menu">
					<li>
						<Link to="#"><i className="fa fa-home"></i> Restaurants <span className="fa fa-chevron-down"></span></Link>
						<ul className="nav child_menu">
							<li className={getNavLinkClass("/")}><NavLink to="/" exact ><i className="fa fa-laptop"></i>Restaurants</NavLink></li>
							<li className={getNavLinkClass("/Categories")}><NavLink to="/Categories" exact ><i className="fa fa-laptop"></i>Categories</NavLink></li>
							<li className={getNavLinkClass("/Menus")}><NavLink to="/Menus" ><i className="fa fa-laptop"></i>Menus</NavLink></li>
							<li className={getNavLinkClass("/Foods")}><NavLink to="/Foods" ><i className="fa fa-laptop"></i>Foods</NavLink></li>
						</ul>
					</li>
					{/* <li className={getNavLinkClass("/")}><NavLink to="/" exact ><i className="fa fa-laptop"></i> Categories</NavLink></li> */}
					{/* <li className={getNavLinkClass("/Menus")}><NavLink to="/Menus" ><i className="fa fa-laptop"></i> Menus </NavLink></li> */}
					{/* <li className={getNavLinkClass("/Foods")}><NavLink to="/Foods" ><i className="fa fa-laptop"></i> Foods </NavLink></li> */}
					<li className={getNavLinkClass("/Customers")}><NavLink to="/Customers" ><i className="fa fa-laptop"></i> Customers </NavLink></li>
					<li className={getNavLinkClass("/Drivers")}><NavLink to="/Drivers" ><i className="fa fa-laptop"></i> Drivers</NavLink></li>
					
				</ul>
			</div>
		</div>
	)
}

export default withRouter(Sidemenu);