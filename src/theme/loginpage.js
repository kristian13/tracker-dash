import React from 'react';
import axios from 'axios';
import {Link} from 'react-router-dom';
import {connect} from 'react-redux';


import * as Yup from 'yup';
import { useFormik } from 'formik';

function LoginPage(props) {

   React.useEffect(()=>{
    document.body.style.backgroundColor = "#FFF";
   });

   const formik = useFormik({
        initialValues : { 
            username : '',
            password : ''
        },
        validationSchema : Yup.object({
            username : Yup.string().required('Required'),
            password : Yup.string().required('Required')
        }),
        onSubmit: (values,{setErrors}) => {
            //Prepare values
            let data  = JSON.stringify({
                'emailAddress' : values.username,
                'password' : values.password
            });

            axios({
                url : 'auth',
                method : 'post',
                headers : { 
                    'Content-Type': 'application/json',
                    'Access-Control-Allow-Origin': '*',
                    'Access-Control-Allow-Methods' : 'GET,PUT,POST,DELETE,PATCH,OPTIONS',
                },
                data: data
            }).then( result => {
                let data = result.data;
                if(data.message === 'success'){
                    localStorage.setItem('user_data',JSON.stringify(data.data));
                    props.setLogin(data.data);
                }
            }).catch(error => {
                //error catch
            });
        }
   });
  return(
    <div className="login">  
        <div className="login_wrapper">
            <div className="animate form login_form">
                <section className="login_content">
                    <form onSubmit={formik.handleSubmit}>
                    <h1>GoRockyGo</h1>
                    <div>
                        {formik.touched.username && formik.errors.username ? (
                            <div className="text-danger">{formik.errors.username}</div>
                        ) : null}  
                        <input 
                            name="username"
                            type="text" 
                            className="form-control" 
                            value={formik.values.username} 
                            onChange={formik.handleChange} 
                            placeholder="Username" 
                            required=""
                        />
                    </div>
                    <div>
                        {formik.touched.password && formik.errors.password ? (
                            <div className="text-danger">{formik.errors.password}</div>
                        ) : null}
                        <input 
                            name="password"
                            type="password" 
                            className="form-control" 
                            value={formik.values.password} 
                            onChange={formik.handleChange} 
                            placeholder="Password" 
                            required=""
                        />
                    </div>
                    <div>
                        <button  type="submit" className="btn btn-default submit" >Log in</button>
                        <Link className="reset_pass" to="#">Lost your password?</Link>
                    </div>

                    <div className="clearfix"></div>

                    <div className="separator hide">
                        <p className="change_link">New to site?
                        <Link to="#signup" className="to_register"> Create Account </Link>
                        </p>

                        <div className="clearfix"></div>
                        <br/>

                        <div>
                        <h1><i className="fa fa-paw"></i> Gentelella Alela!</h1>
                        <p>©2016 All Rights Reserved. Gentelella Alela! is a Bootstrap 3 template. Privacy and Terms</p>
                        </div>
                    </div>
                    </form>
                </section>
            </div>

            <div id="register" className="animate form registration_form">
            <section className="login_content">
                <form>
                <h1>Create Account</h1>
                <div>
                    <input type="text" className="form-control" placeholder="Username" required=""/>
                </div>
                <div>
                    <input type="email" className="form-control" placeholder="Email" required=""/>
                </div>
                <div>
                    <input type="password" className="form-control" placeholder="Password" required=""/>
                </div>
                <div>
                    <Link className="btn btn-default submit" to="index.html">Submit</Link>
                </div>
                <div className="clearfix"></div>
                <div className="separator">
                    <p className="change_link">Already a member ?
                    <Link to="#signin" className="to_register"> Log in </Link>
                    </p>

                    <div className="clearfix"></div>
                    <br/>

                    <div>
                    <h1><i className="fa fa-paw"></i> Gentelella Alela!</h1>
                    <p>©2016 All Rights Reserved. Gentelella Alela! is a Bootstrap 3 template. Privacy and Terms</p>
                    </div>
                </div>
                </form>
            </section>
            </div>
        </div>
    </div>
  );
  
}

const mapStateProps = (state) => {
    return {
        state: state
    }
}

const mapDispatchProps = (dispatch) => {
    return {
        setLogin:(payload) => dispatch({type:'AUTH_LOGIN',payload:payload})
    }
}
export default connect(mapStateProps,mapDispatchProps)(LoginPage);