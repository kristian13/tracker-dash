import React from 'react';
import { connect } from 'react-redux';
import Pagination from "react-js-pagination";
import axios from 'axios';
import { Link } from "react-router-dom";
import { AddModal, EditModal } from './modal';

import { RestaurantList } from './helper';


class Restaurants extends React.Component {

	_isMounted = false;
	constructor(props) {
		super(props);

		this.state = {
			modalType: '',
			modalData: {
				imageURL: '',
				isActive: '',
				name: '',
				description: '',
				slug: '',
			},
			tags: [],
			suggestions: [],
			categories: []
		}
		

		this.handleDelete = this.handleDelete.bind(this);
		this.handleAddition = this.handleAddition.bind(this);
		this.handleDrag = this.handleDrag.bind(this);

		this.handlePageChange = this.handlePageChange.bind(this);
	}

	//LIFECYCLE
	componentDidMount() {
		this._isMounted = true;
		axios({
			url: `/restaurants/all?sort=&order=&page=&limit=${this.props.restaurants.pagination.currentLimit}`,
			method: 'GET',
		}).then(result => {
			let data = result.data.data;
			if(!this._isMounted) {return false;}
			this.props.restaurant_page_init(data);
		}).catch(error => {
			console.log(error);
		});

		//Categories
		axios.get('/categories/?sort=&order=&page=&limit')
			.then((result) => {
				let data = result.data.data;
				if(!this._isMounted) {return false;}
				
				//get only name object
				let cats = data.categories.map((item,k) => {
					let d = {id:item.name,text:item.name,_id:item._id}
					return d;
				});

				//SET SUGGESTION
				this.setState({
					suggestions: [...cats],
					categories: [...cats]
				})
			});
	}
	componentWillUnmount() {
		this._isMounted = false;
	}
	// END LIFECYCLE
	
	badRequest = (error) => {
        if (error.response !== undefined) {
            let e = error.response.data;
            if (e.status === 400) {
				if(e.data.error) {
					switch(e.data.error.message) {
						case 'jwt expired':
							alert('TOKEN EXPIRED LOGIN AGAIN...');
							this.props.setLogout();
						break;
						default:
						// code block
				  	}
				}
            }
        }
	}
	

    SetActive = (event) => {
        this.setState((prevState) => ({
            modalData: {
                ...prevState.modalData,
                isActive: !this.state.modalData.isActive
            }
        }));
    }

	UpdateState = (data,action) => {
		const { restaurants } = this.props.restaurants; 

		//ADD
		if(action === 'add'){
			
			let cats = this.props.categories.categories.filter(function(item){
				if(data.categories.includes(item._id)){
					return item;
				}
				return false;
			});
			
			let newData = { ...data, categories: [...cats] };
			newData = [{...newData},...restaurants];
			this.props.restaurant_add(newData);
			
		} //EDIT 
		else {
			
			let update = restaurants.map((item) => {
				if(item._id === data._id){		
					return {...data}
				}
				return item;
			});
			
			this.props.restaurant_edit(update);
		}

		this.closeModal();
	}

	// FOR TAGS
		handleDelete(i,e) {
			//Disable Backspace
			if(e.keyCode === 8 ){return false;}

			const { tags } = this.state;
			this.setState({
				tags: tags.filter((tag, index) => index !== i),
			});
		}
	
		handleAddition(tag) {
			this.setState(state => ({ tags: [...state.tags, tag] }));
		}
	
		handleDrag(tag, currPos, newPos) {
			const tags = [...this.state.tags];
			const newTags = tags.slice();
	
			newTags.splice(currPos, 1);
			newTags.splice(newPos, 0, tag);
	
			// re-render
			this.setState({ tags: newTags });
		}
	// END FOR TAGS


	ShowModal = (props, e) => {
		let action = e.target.getAttribute('action');
		let cats = [];

		if(props.categories){
			cats = props.categories.map((item,k) => {
				return {id:item.name,text:item.name,_id:item._id};
			});
		} else {
			cats = [];
		}

		this.setState({
			modalData: { ...props },
			modalDisplay: 'block',
			modalType: action,
			tags : cats
		});
	}

	handlePageChange = (pageNumber) => {
        axios.get(`/restaurants/all?sort=&order=&page=${pageNumber}&limit=${this.props.restaurants.pagination.currentLimit}`)
        .then((result) => {
			let data = result.data.data;
			this.props.restaurant_page_update(data);
        });
    }

	closeModal = () => {
		this.setState((prevState, props) => ({
			...prevState,
			modalDisplay: 'none',
			modalType: '',
			modalData: {},
			tags: [],
		}));
	}

	render() {
		return (
			<div>
				<div>
					{this.state.modalDisplay !== 'none' && this.state.modalType === 'add' ? (
						<AddModal
							modalDisplay={this.state.modalDisplay}
							modalData={this.state.modalData}
							UpdateState={this.UpdateState}
							// TAGS EVENTS 
							tags={this.state.tags}
							suggestions={this.state.suggestions}
							handleDelete={this.handleDelete}
							handleAddition={this.handleAddition}
							handleDrag={this.handleDrag}
							handleTagClick={this.handleTagClick}
							// END TAGS EVENTS
							SetActive={this.SetActive}
							logOut={this.props.state.setLogout}
							badRequest={this.badRequest}
							closeModal={this.closeModal}
						/>
					) : null}

					{this.state.modalDisplay !== 'none' && this.state.modalType === 'edit' ? (
						<EditModal
							modalDisplay={this.state.modalDisplay}
							modalData={this.state.modalData}
							UpdateState={this.UpdateState}
							categories={this.props.categories}
							logOut={this.props.state.setLogout}
							// TAGS EVENTS 
							tags={this.state.tags}
							suggestions={this.state.suggestions}
							handleDelete={this.handleDelete}
							handleAddition={this.handleAddition}
							handleDrag={this.handleDrag}
							handleTagClick={this.handleTagClick}
							// END TAGS EVENTS
							restaurant_edit_image={this.props.restaurant_edit_image}
							SetActive={this.SetActive}
							badRequest={this.badRequest}
							closeModal={this.closeModal}
						/>
					) : null}
				</div>
				<div className="col-md-12 col-sm-6 col-xs-12">
					<div className="page-title">
						<div className="title_left">
							<br />
							<button type="button" className="btn btn-success btn-sm" onClick={this.ShowModal.bind(this, '')} action="add">
								Add Restaurant
                            </button>
							<div className="clearfix"></div>
						</div>
						<div className="title_right">
							<div className="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
								<div className="input-group">
									<input type="text" className="form-control" placeholder="Search for..." />
									<span className="input-group-btn">
										<button className="btn btn-default" type="button">Go!</button>
									</span>
								</div>
							</div>
						</div>
					</div>
					<div className="x_panel">
						<div className="x_title">
							<h2><i className="fa fa-bars"></i> Restaurants</h2>
							<ul className="nav navbar-right panel_toolbox hide">
								<li><Link to="" className="collapse-link"><i className="fa fa-chevron-up"></i></Link></li>
								<li className="dropdown">
									<Link to="" className="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i className="fa fa-wrench"></i></Link>
									<ul className="dropdown-menu" role="menu">
										<li><Link to="" >Settings 1</Link></li>
										<li><Link to="">Settings 2</Link></li>
									</ul>
								</li>
								<li><Link to="" className="close-link"><i className="fa fa-close"></i></Link></li>
							</ul>
							<div className="clearfix"></div>
						</div>
						<div className="x_content">
							<table className="table table-hover">
								<thead>
									<tr>
										<th>Business Info</th>
										<th>Address</th>
										<th>Operations</th>
										<th>Status</th>
										<th>Categories</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
									{this.props.restaurants.restaurants.length > 0 ? (
										this.props.restaurants.restaurants.map((obj, key) =>
											<RestaurantList
												key={key}
												{...obj}
												edit={this.ShowModal.bind(this, obj)}
											/>
										)
									) : null}
								</tbody>
							</table>
						</div>
					</div>
				</div>
				
				<div className="col-lg-12">
                    <Pagination
                        innerClass="pagination pull-right" 
                        hideDisabled
                        activePage={this.props.restaurants.pagination.currentPage}
                        itemsCountPerPage={this.props.restaurants.pagination.currentLimit}
                        totalItemsCount={this.props.restaurants.pagination.recordCount}
                        onChange={this.handlePageChange}
                    />
                </div>
			</div>
		)
	}
}

const mapStateToProps = (state) => {
	return {
		state: state,
		categories: state.categories,
		restaurants: state.restaurants
	}
}

const mapDispatchToProps = (dispatch) => {
	return {
		restaurant_page_init   :(data) => dispatch({ type:'RESTAURANT_INIT'        , payload: data }),
		restaurant_page_update :(data) => dispatch({ type:'RESTAURANT_PAGE_UPDATE' , payload: data }),
		restaurant_add         :(data) => dispatch({ type:'RESTAURANT_ADD'         , payload:data }),
		restaurant_edit        :(data) => dispatch({ type:'RESTAURANT_EDIT'        , payload:data }),
		restaurant_edit_image  :(data) => dispatch({ type:'RESTAURANT_EDIT_IMAGE'  , payload:data }),
		setLogout              :(data) => dispatch({ type:'AUTH_LOGOUT'            , payload: data })
	}
}
export default connect(mapStateToProps, mapDispatchToProps)(Restaurants);