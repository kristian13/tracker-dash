import React from 'react';

export const RestaurantList = (props) => {
    return (
        <tr>
            <td>
                <div className="row">
                    <div className="col-md-12 col-sm-12 col-xs-12 profile_details">
                        <div className="col-sm-12">
                            {props.imageURL ? (
                                <div>
                                    <img alt="" className="img-thumbnail" src={props.imageURL} height="120" width="150"/>
                                </div>
                            )
                            : null}
                            <h4 className="brief"><i>{props.businessName}</i></h4>
                            <div className="left col-sm-12">
                                <ul className="list-unstyled">
                                    <li><i className="fa fa-phone"></i> Mobile #: {props.mobileNumber}</li>
                                    <li><i className="fa fa-phone"></i> Phone #: {props.phoneNumber}</li>
                                </ul>
                            </div>
                           
                        </div>
                    </div>
                </div>
            </td>
            <td>
                <div className="row">
                    <div className="col-md-12 col-sm-12 col-xs-12">
                        {props.address ? (
                            <ul className="list-unstyled">
                                <li><i className=""></i>{props.address.number ? props.address.number : null}</li>
                                <li><i className=""></i>{props.address.building ? props.address.building : null}</li>
                                <li><i className=""></i>{props.address.street ? props.address.street : null}</li>
                                <li><i className=""></i>{props.address.district ? props.address.district : null}</li>
                                <li><i className=""></i>{props.address.city ? props.address.city : null}</li>
                                <li><i className=""></i>{props.address.state ? props.address.state : null}</li>
                                <li><i className=""></i>{props.address.country ? props.address.country : null}</li>
                            </ul>
                        ) : null}
                    </div>
                </div>
            </td>
            <td>
                <div className="row">
                    <div className="col-md-12 col-sm-12 col-xs-12">
                        <ul className="list-unstyled">
                            <li><i className="fa fa-clock-o"></i> Opening Time : {props.openingTime}</li>
                            <li><i className="fa fa-clock-o"></i> Operating Hours : {props.operatingHours}</li>
                            <li><i className="fa fa-reorder"></i> Minimum Order : {props.minimumOrder}</li>
                            <li><i className="fa fa-car"></i> Estimated Delivery Time : {props.estimatedDeliveryTime}</li>
                        </ul>
                    </div>
                </div>
            </td>
            <td>
                {props.isActive ? (<span className="label label-success">Active</span>) : (<span class="label label-danger">In-active</span>)}
            </td>
            <td>
                {props.categories.length > 0 ?
                    props.categories.map((i,k) => <span key={k} className="label label-primary"> {i.name} </span> )
                : (<h5>No Category</h5>)}
            </td>
            <td>
                <button onClick={props.edit.bind(this)} action="edit" className="buttonPrevious btn btn-xs btn-primary">Edit</button>
            </td>
        </tr>
    )
}

export const Tags = (props) => {
    return (
        <span>props.name</span>
    )
}

