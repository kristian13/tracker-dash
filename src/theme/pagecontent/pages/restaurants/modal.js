import React from 'react';
import { useFormik, Formik, Form} from 'formik';
import * as Yup from 'yup';

import axios from 'axios';

import { WithContext as ReactTags } from 'react-tag-input';
import './modal.css';


export const AddModal = (props) => {

    const formik = useFormik({
        // load data init 
        initialValues: {
            //Business INFO
            userName: '',
            emailAddress: '',
            firstName: '',
            lastName: '',
            businessName: '',
            mobileNumber: '',
            phoneNumber: '',
            password:'',

            //Address
            number: '',
            building: '',
            street: '',
            district: '',
            city: '',
            state: '',
            zipCode: '',
            country: '',

            //Operations
            openingTime: '',
            closingTime: '',
            estimatedDeliveryTime: '',
            minimumOrder: '',
            deliveryFee: ''
        },

        // validation filter
        validationSchema: Yup.object({
            //Business Info
            businessName: Yup.string()
                .min(1, 'Must be 15 characters or less')
                .required('Required'),
            phoneNumber: Yup.string()
                .min(1, 'Must be 15 characters or less')
                .required('Required'),
            mobileNumber: Yup.string()
                .min(1, 'Must be 15 characters or less')
                .required('Required'),
            userName: Yup.string()
                .min(1, 'Must be 15 characters or less')
                .required('Required'),
            emailAddress: Yup.string()
                .min(1, 'Must be 15 characters or less')
                .required('Required'),
            firstName: Yup.string()
                .min(1, 'Must be 15 characters or less')
                .required('Required'),
            lastName: Yup.string()
                .min(1, 'Must be 15 characters or less')
                .required('Required'),
            password: Yup.string()
                .required('No password provided.') 
                .min(8, 'Password is too short - should be 8 chars minimum.'),
            /* missing
                "deviceId":"dashboard",
                "platform":"dashboard",
                "userType":"restaurant",
                "isUpdated":false,
                "password":"trackresto1234"
            */

            //Address
            number: Yup.string(),
            building: Yup.string(),
            district: Yup.string(),

            city: Yup.string(),
            state: Yup.string(),
            zipCode: Yup.string(),
            country: Yup.string(),

            //Operations
            /* missing
                deliveryFee
            */
            openingTime: Yup.string(),
            operatingHours: Yup.string(),
            estematedDeliveryTime: Yup.string(),
            minimumOrder: Yup.string()

        }),

        // submit function
        onSubmit: values => {
           
            let data = JSON.stringify({
                "user":
                {
                  
                    "userName": values.userName,
                    "emailAddress": values.emailAddress,
                    "firstName": values.firstName,
                    "lastName": values.lastName,
                    "businessName": values.businessName,
                    "phoneNumber": values.phoneNumber,
                    "mobileNumber": values.mobileNumber,
                    "userType" :"restaurant",
                    "deviceId":"dashboard",
                    "platform":"dashboard",
                    "isUpdated":false,
                    "password":values.password
                },
                "address":
                {
                   
                    "number": values.number,
                    "building": values.building,
                    "street": values.street,
                    "district": values.district,
                    "city": values.city,
                    "state": values.state,
                    "zipCode": values.zipCode,
                    "country": values.country,
                    "type" : 'business'
                },
                "operations":
                {
                    "openingTime": values.openingTime,
                    "closingTime": values.closingTime,
                    "estimatedDeliveryTime": values.estimatedDeliveryTime,
                    "minimumOrder": values.minimumOrder,
                    "deliveryFee": values.deliveryFee
                },
                "categories": props.tags.map((item, k) => { return item._id; })
            });

            axios({
                url: '/restaurants/',
                method: 'POST',
                data: data
            }).then(result => {
                if(result.data.status === 200){
                    props.UpdateState(result.data.data, 'add' );
                }
            }).catch(error => {
                props.badRequest(error);
            });

        }
    });

    return (
        <div className="modal " style={{ display: props.modalDisplay }}>
            <div className=" modal-dialog modal-lg">
                <div className="modal-content">
                    <div className="modal-header">
                        <span className="close" onClick={props.closeModal}>&times;</span>
                        <h2><i style={{ color: 'white' }}>Add Restaurant</i></h2>
                    </div>
                    <div className="modal-body">
                        <form onSubmit={formik.handleSubmit}>
                            <div className="" role="tabpanel" data-example-id="togglable-tabs">
                                <ul id="myTab" className="nav nav-tabs bar_tabs" role="tablist">
                                    <li role="presentation" className="active"><a href="#tab_content1" id="business-tab" role="tab" data-toggle="tab" aria-expanded="false">Business Info</a>
                                    </li>
                                    <li role="presentation" className=""><a href="#tab_content2" role="tab" id="address-tab" data-toggle="tab" aria-expanded="false">Address</a>
                                    </li>
                                    <li role="presentation" className=""><a href="#tab_content3" role="tab" id="operation-tab" data-toggle="tab" aria-expanded="false">Operation</a>
                                    </li>
                                </ul>
                                <div id="myTabContent" className="tab-content">
                                    <div role="tabpanel" className="tab-pane active fade in" id="tab_content1" aria-labelledby="business-tab">
                                        <div>
                                            <label htmlFor="businessName">Business Name * :</label>
                                            <input
                                                id="businessName"
                                                name="businessName"
                                                type="text"
                                                onChange={formik.handleChange}
                                                onBlur={formik.handleBlur}
                                                value={formik.values.businessName}
                                                className="form-control"
                                            />
                                            {formik.touched.businessName && formik.errors.businessName ? (
                                                <div className="text-danger">{formik.errors.businessName}
                                                </div>
                                            ) : null}
                                        </div>
                                        <div>
                                            <label htmlFor="phoneNumber">Phone Number * :</label>
                                            <input
                                                id="phoneNumber"
                                                name="phoneNumber"
                                                type="text"
                                                onChange={formik.handleChange}
                                                onBlur={formik.handleBlur}
                                                value={formik.values.phoneNumber}
                                                className="form-control"
                                            />
                                            {formik.touched.phoneNumber && formik.errors.phoneNumber ? (
                                                <div className="text-danger">{formik.errors.phoneNumber}
                                                </div>
                                            ) : null}
                                        </div>
                                        <div>
                                            <label htmlFor="mobileNumber">Mobile Number * :</label>
                                            <input
                                                id="mobileNumber"
                                                name="mobileNumber"
                                                type="text"
                                                onChange={formik.handleChange}
                                                onBlur={formik.handleBlur}
                                                value={formik.values.mobileNumber}
                                                className="form-control"
                                            />
                                            {formik.touched.mobileNumber && formik.errors.mobileNumber ? (
                                                <div className="text-danger">{formik.errors.mobileNumber}
                                                </div>
                                            ) : null}
                                            <div>
                                                <div>
                                                    <label htmlFor="mobileNumber">Categories :</label>
                                                    <div className="col-lg-12">
                                                        <ReactTags
                                                            inputFieldPosition="bottom"
                                                            tags={props.tags}
                                                            suggestions={props.suggestions}
                                                            handleDelete={props.handleDelete}
                                                            handleAddition={props.handleAddition}
                                                            handleTagClick={props.handleTagClick}
                                                            delimiters={delimiters}
                                                            placeholder="Add Category"
                                                        />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <h4>User Info</h4>
                                        <div>
                                            <label htmlFor="userName">Username * :</label>
                                            <input
                                                id="userName"
                                                name="userName"
                                                type="text"
                                                onChange={formik.handleChange}
                                                onBlur={formik.handleBlur}
                                                value={formik.values.userName}
                                                className="form-control"
                                            />
                                            {formik.touched.userName && formik.errors.userName ? (
                                                <div className="text-danger">{formik.errors.userName}
                                                </div>
                                            ) : null}
                                        </div>
                                        <div>
                                            <label htmlFor="emailAddress">Email Address * :</label>
                                            <input
                                                id="emailAddress"
                                                name="emailAddress"
                                                type="text"
                                                onChange={formik.handleChange}
                                                onBlur={formik.handleBlur}
                                                value={formik.values.emailAddress}
                                                className="form-control"
                                            />
                                            {formik.touched.emailAddress && formik.errors.emailAddress ? (
                                                <div className="text-danger">{formik.errors.emailAddress}
                                                </div>
                                            ) : null}
                                        </div>
                                        <div>
                                            <label htmlFor="password">Password * :</label>
                                            <input
                                                id="password"
                                                name="password"
                                                type="password"
                                                onChange={formik.handleChange}
                                                onBlur={formik.handleBlur}
                                                value={formik.values.password}
                                                className="form-control"
                                            />
                                            {formik.touched.password && formik.errors.password ? (
                                                <div className="text-danger">{formik.errors.password}
                                                </div>
                                            ) : null}
                                        </div>
                                        <div>
                                            <label htmlFor="firstName">First Name* :</label>
                                            <input
                                                id="firstName"
                                                name="firstName"
                                                type="text"
                                                onChange={formik.handleChange}
                                                onBlur={formik.handleBlur}
                                                value={formik.values.firstName}
                                                className="form-control"
                                            />
                                            {formik.touched.firstName && formik.errors.firstName ? (
                                                <div className="text-danger">{formik.errors.firstName}
                                                </div>
                                            ) : null}
                                        </div>
                                        <div>
                                            <label htmlFor="lastName">Last Name * :</label>
                                            <input
                                                id="lastName"
                                                name="lastName"
                                                type="text"
                                                onChange={formik.handleChange}
                                                onBlur={formik.handleBlur}
                                                value={formik.values.lastName}
                                                className="form-control"
                                            />
                                            {formik.touched.lastName && formik.errors.lastName ? (
                                                <div className="text-danger">{formik.errors.lastName}
                                                </div>
                                            ) : null}
                                        </div>
                                    </div>
                                    <div role="tabpanel" className="tab-pane fade" id="tab_content2" aria-labelledby="address-tab">

                                        <div>
                                            <label htmlFor="number">Number :</label>
                                            <input
                                                id="number"
                                                name="number"
                                                type="text"
                                                onChange={formik.handleChange}
                                                onBlur={formik.handleBlur}
                                                value={formik.values.number}
                                                className="form-control"
                                            />
                                            {formik.touched.number && formik.errors.number ? (
                                                <div className="text-danger">{formik.errors.number}

                                                </div>
                                            ) : null}
                                        </div>
                                        <div>
                                            <label htmlFor="building">Building :</label>
                                            <input
                                                id="building"
                                                name="building"
                                                type="text"
                                                onChange={formik.handleChange}
                                                onBlur={formik.handleBlur}
                                                value={formik.values.building}
                                                className="form-control"
                                            />
                                            {formik.touched.building && formik.errors.building ? (
                                                <div className="text-danger">{formik.errors.building}</div>
                                            ) : null}
                                        </div>
                                        <div>
                                            <label htmlFor="district">District Name :</label>
                                            <input
                                                id="district"
                                                name="district"
                                                type="text"
                                                onChange={formik.handleChange}
                                                onBlur={formik.handleBlur}
                                                value={formik.values.district}
                                                className="form-control"
                                            />
                                            {formik.touched.district && formik.errors.district ? (
                                                <div className="text-danger">{formik.errors.district}</div>
                                            ) : null}
                                        </div>
                                        <div>
                                            <label htmlFor="city">city Name :</label>
                                            <input
                                                id="city"
                                                name="city"
                                                type="text"
                                                onChange={formik.handleChange}
                                                onBlur={formik.handleBlur}
                                                value={formik.values.city}
                                                className="form-control"
                                            />
                                            {formik.touched.city && formik.errors.city ? (
                                                <div className="text-danger">{formik.errors.city}</div>
                                            ) : null}
                                        </div>
                                        <div>
                                            <label htmlFor="state">State Name :</label>
                                            <input
                                                id="state"
                                                name="state"
                                                type="text"
                                                onChange={formik.handleChange}
                                                onBlur={formik.handleBlur}
                                                value={formik.values.state}
                                                className="form-control"
                                            />
                                            {formik.touched.state && formik.errors.state ? (
                                                <div className="text-danger">{formik.errors.state}</div>
                                            ) : null}
                                        </div>
                                        <div>
                                            <label htmlFor="zipCode">zipCode :</label>
                                            <input
                                                id="zipCode"
                                                name="zipCode"
                                                type="text"
                                                onChange={formik.handleChange}
                                                onBlur={formik.handleBlur}
                                                value={formik.values.zipCode}
                                                className="form-control"
                                            />
                                            {formik.touched.zipCode && formik.errors.zipCode ? (
                                                <div className="text-danger">{formik.errors.zipCode}</div>
                                            ) : null}
                                        </div>
                                        <div>
                                            <label htmlFor="businessName">Country Name :</label>
                                            <input
                                                id="country"
                                                name="country"
                                                type="text"
                                                onChange={formik.handleChange}
                                                onBlur={formik.handleBlur}
                                                value={formik.values.country}
                                                className="form-control"
                                            />
                                            {formik.touched.country && formik.errors.country ? (
                                                <div className="text-danger">{formik.errors.country}</div>
                                            ) : null}
                                        </div>

                                    </div>
                                    <div role="tabpanel" className="tab-pane fade" id="tab_content3" aria-labelledby="operation-tab">
                                        <div>
                                            <label htmlFor="openingTime">Opening Time :</label>
                                            <input
                                                id="openingTime"
                                                name="openingTime"
                                                type="openingTime"
                                                onChange={formik.handleChange}
                                                onBlur={formik.handleBlur}
                                                value={formik.values.openingTime}
                                                className="form-control"
                                            />
                                            {formik.touched.openingTime && formik.errors.openingTime ? (
                                                <div className="text-danger">{formik.errors.openingTime}</div>
                                            ) : null}
                                        </div>
                                        <div>
                                            <label htmlFor="operatingHours">Operating Hours :</label>
                                            <input
                                                id="operatingHours"
                                                name="operatingHours"
                                                type="operatingHours"
                                                onChange={formik.handleChange}
                                                onBlur={formik.handleBlur}
                                                value={formik.values.operatingHours}
                                                className="form-control"
                                            />
                                            {formik.touched.operatingHours && formik.errors.operatingHours ? (
                                                <div className="text-danger">{formik.errors.operatingHours}</div>
                                            ) : null}
                                        </div>
                                        <div>
                                            <label htmlFor="estimatedDeliveryTime">Estemated Delivery Time :</label>
                                            <input
                                                id="estimatedDeliveryTime"
                                                name="estimatedDeliveryTime"
                                                type="estimatedDeliveryTime"
                                                onChange={formik.handleChange}
                                                onBlur={formik.handleBlur}
                                                value={formik.values.estimatedDeliveryTime}
                                                className="form-control"
                                            />
                                            {formik.touched.estimatedDeliveryTime && formik.errors.estimatedDeliveryTime ? (
                                                <div className="text-danger">{formik.errors.estimatedDeliveryTime}</div>
                                            ) : null}
                                        </div>
                                        <div>
                                            <label htmlFor="minimumOrder">Minimum Order :</label>
                                            <input
                                                id="minimumOrder"
                                                name="minimumOrder"
                                                type="minimumOrder"
                                                onChange={formik.handleChange}
                                                onBlur={formik.handleBlur}
                                                value={formik.values.minimumOrder}
                                                className="form-control"
                                            />
                                            {formik.touched.minimumOrder && formik.errors.minimumOrder ? (
                                                <div className="text-danger">{formik.errors.minimumOrder}</div>
                                            ) : null}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br />
                            <button type="submit" className="btn btn-success pull-right" disabled={formik.isValid ? false : true}>Submit</button>
                        </form>
                    </div>
                </div>
            </div>
        </div >
    )
}


const KeyCodes = {comma: 188,enter: 13};
const delimiters = [KeyCodes.comma, KeyCodes.enter];
// Make sure state data is set before calling the modal
// if not the initialValue will be null
export const EditModal = (props) => {


    const initialValue = {
                ...props.modalData,
                //Business INFO
                emailAddress: props.modalData.user.emailAddress,
                userName: props.modalData.user.userName,
    
                //Address
                number: props.modalData.address ? props.modalData.address.number : '',
                building: props.modalData.address ? props.modalData.address.building : '',
                district: props.modalData.address ? props.modalData.address.district : '',
                city: props.modalData.address ? props.modalData.address.city : '',
                state: props.modalData.address ? props.modalData.address.state : '',
                zipCode: props.modalData.address ? props.modalData.address.zipCode : '',
                country: props.modalData.address ? props.modalData.address.country : '',
    
                //Operations
                deliveryFee: ''
     }

    const formSchema = Yup.object({
        //Business Info
        businessName: Yup.string()
            .min(1, 'Must be 15 characters or less')
            .required('Required'),
        phoneNumber: Yup.string()
            .min(1, 'Must be 15 characters or less')
            .required('Required'),
        mobileNumber: Yup.string()
            .min(1, 'Must be 15 characters or less')
            .required('Required'),
        userName: Yup.string()
            .min(1, 'Must be 15 characters or less')
            .required('Required'),
        emailAddress: Yup.string()
            .min(1, 'Must be 15 characters or less')
            .required('Required'),
        firstName: Yup.string()
            .min(1, 'Must be 15 characters or less')
            .required('Required'),
        lastName: Yup.string()
            .min(1, 'Must be 15 characters or less')
            .required('Required'),

        /* missing
            "deviceId":"dashboard",
            "platform":"dashboard",
            "userType":"restaurant",
            "isUpdated":false,
            "password":"trackresto1234"
        */

        //Address
        number: Yup.string(),
        building: Yup.string(),
        district: Yup.string(),

        city: Yup.string(),
        state: Yup.string(),
        zipCode: Yup.string(),
        country: Yup.string(),

        //Operations
        /* missing
            deliveryFee
        */
        openingTime: Yup.string(),
        operatingHours: Yup.string(),
        estematedDeliveryTime: Yup.string(),
        minimumOrder: Yup.string()

    });

    let isActive = props.modalData.isActive ? 'checked' : '';

    return (
        <div className="modal " style={{ display: props.modalDisplay }}>
            <div className=" modal-dialog modal-lg">
                <div className="modal-content">
                    <div className="modal-header">
                        <span className="close" onClick={props.closeModal}>&times;</span>
                        <h2><i style={{ color: 'white' }}>Edit Restaurant</i></h2>
                    </div>
                    <div className="modal-body">
                        <Formik 
                                initialValues={initialValue} 
                                validationSchema={formSchema}
                                onSubmit={(values) => {

                                    if(values.File){
                                        const data = new FormData(); 
                                        data.append('restaurant', values.File);
                                        data.append('_id', props.modalData._id);
                                        axios({
                                            url : "upload/restaurant",
                                            method : 'POST',
                                            data : data,
                                        }).then( result => {
                                            if(result.status === 200){
                                                props.restaurant_edit_image({
                                                    _id:props.modalData._id,
                                                    imageURL: result.data.data.imageURL,
                                                });
                                            }
                                        }).catch( e => {
                                        
                                        });
                                    }

                                    let rawData = JSON.stringify({
                                        "user":
                                        {
                                            "_id": values.user._id,
                                            "userName": values.userName,
                                            "emailAddress": values.emailAddress,
                                            "firstName": values.firstName,
                                            "lastName": values.lastName,
                                            "businessName": values.businessName,
                                            "mobileNumber": values.mobileNumber,
                                            "phoneNumber": values.phoneNumber
                                        },
                                        "address":
                                        {
                                            "_id": values.address._id,
                                            "number": values.number,
                                            "building": values.building,
                                            "street": values.street,
                                            "district": values.district,
                                            "city": values.city,
                                            "state": values.state,
                                            "zipCode": values.zipCode,
                                            "country": values.country
                                        },
                                        "operations":
                                        {
                                            "openingTime": values.openingTime,
                                            "closingTime": values.closingTime,
                                            "estimatedDeliveryTime": values.estimatedDeliveryTime,
                                            "minimumOrder": values.minimumOrder,
                                            "deliveryFee": values.deliveryFee
                                        },
                                        "categories": props.tags.map((item, k) => { return item._id; })
                                    });

                                    axios({
                                        url: '/restaurants/' + props.modalData._id,
                                        method: 'PUT',
                                        data: rawData
                                    }).then(result => {
                                        if(result.data.status === 200){
                                            let newData = {
                                                ...props.modalData,
                                                ...result.data.data,
                                                address: {...props.modalData.address,...result.data.data.address},
                                                user   : {...props.modalData.user,...result.data.data.user},
                                            }
                                            props.UpdateState(newData, 'edit');
                                        }
                                    }).catch(error => {
                                        props.badRequest(error);
                                    });

                                }}
                        > 
                            {({ values, touched, errors, handleSubmit, handleChange, handleBlur, setFieldValue, isValid }) => (
                                <Form onSubmit={handleSubmit} encType="multipart/form-data">
                                    <div className="" role="tabpanel" data-example-id="togglable-tabs">
                                        <ul id="myTab" className="nav nav-tabs bar_tabs" role="tablist">
                                            <li role="presentation" className="active"><a href="#tab_content1" id="business-tab" role="tab" data-toggle="tab" aria-expanded="false">Business Info</a>
                                            </li>
                                            <li role="presentation" className=""><a href="#tab_content2" role="tab" id="address-tab" data-toggle="tab" aria-expanded="false">Address</a>
                                            </li>
                                            <li role="presentation" className=""><a href="#tab_content3" role="tab" id="operation-tab" data-toggle="tab" aria-expanded="false">Operation</a>
                                            </li>
                                            <li role="presentation" className=""><a href="#tab_content4" role="tab" id="upload-tab" data-toggle="tab" aria-expanded="false">Upload</a>
                                            </li>
                                        </ul>
                                        <div id="myTabContent" className="tab-content">
                                            <div role="tabpanel" className="tab-pane active fade in" id="tab_content1" aria-labelledby="business-tab">
                                                <div>
                                                    <div className="checkbox pull-right">
                                                        <label className="">
                                                            <div className={`icheckbox_flat-green ${isActive}`} onClick={props.SetActive} style={{Position: 'relative'}}></div> Is Active
                                                        </label>
                                                    </div>
                                                </div>
                                                <div>
                                                    <label htmlFor="businessName">Business Name * :</label>
                                                    <input
                                                        id="businessName"
                                                        name="businessName"
                                                        type="text"
                                                        onChange={handleChange}
                                                        onBlur={handleBlur}
                                                        value={values.businessName}
                                                        className="form-control"
                                                    />
                                                    {touched.businessName && errors.businessName ? (
                                                        <div className="text-danger">{errors.businessName}
                                                        </div>
                                                    ) : null}
                                                </div>
                                                <div>
                                                    <label htmlFor="phoneNumber">Phone Number * :</label>
                                                    <input
                                                        id="phoneNumber"
                                                        name="phoneNumber"
                                                        type="text"
                                                        onChange={handleChange}
                                                        onBlur={handleBlur}
                                                        value={values.phoneNumber}
                                                        className="form-control"
                                                    />
                                                    {touched.phoneNumber && errors.phoneNumber ? (
                                                        <div className="text-danger">{errors.phoneNumber}
                                                        </div>
                                                    ) : null}
                                                </div>
                                                <div>
                                                    <label htmlFor="mobileNumber">Mobile Number * :</label>
                                                    <input
                                                        id="mobileNumber"
                                                        name="mobileNumber"
                                                        type="text"
                                                        onChange={handleChange}
                                                        onBlur={handleBlur}
                                                        value={values.mobileNumber}
                                                        className="form-control"
                                                    />
                                                    {touched.mobileNumber && errors.mobileNumber ? (
                                                        <div className="text-danger">{errors.mobileNumber}
                                                        </div>
                                                    ) : null}
                                                    <div>
                                                        <div>
                                                            <label htmlFor="mobileNumber">Categories :</label>
                                                            <div className="col-lg-12">
                                                                <ReactTags
                                                                    inputFieldPosition="bottom"
                                                                    tags={props.tags}
                                                                    suggestions={props.suggestions}
                                                                    handleDelete={props.handleDelete}
                                                                    handleAddition={props.handleAddition}
                                                                    handleTagClick={props.handleTagClick}
                                                                    delimiters={delimiters}
                                                                    placeholder="Add Category"
                                                                />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <h4>User Info</h4>
                                                <div>
                                                    <label htmlFor="userName">Username * :</label>
                                                    <input
                                                        id="userName"
                                                        name="userName"
                                                        type="text"
                                                        onChange={handleChange}
                                                        onBlur={handleBlur}
                                                        value={values.userName}
                                                        className="form-control"
                                                    />
                                                    {touched.userName && errors.userName ? (
                                                        <div className="text-danger">{errors.userName}
                                                        </div>
                                                    ) : null}
                                                </div>
                                                <div>
                                                    <label htmlFor="emailAddress">Email Address * :</label>
                                                    <input
                                                        id="emailAddress"
                                                        name="emailAddress"
                                                        type="text"
                                                        onChange={handleChange}
                                                        onBlur={handleBlur}
                                                        value={values.emailAddress}
                                                        className="form-control"
                                                    />
                                                    {touched.emailAddress && errors.emailAddress ? (
                                                        <div className="text-danger">{errors.emailAddress}
                                                        </div>
                                                    ) : null}
                                                </div>
                                                <div>
                                                    <label htmlFor="firstName">First Name* :</label>
                                                    <input
                                                        id="firstName"
                                                        name="firstName"
                                                        type="text"
                                                        onChange={handleChange}
                                                        onBlur={handleBlur}
                                                        value={values.firstName}
                                                        className="form-control"
                                                    />
                                                    {touched.firstName && errors.firstName ? (
                                                        <div className="text-danger">{errors.firstName}
                                                        </div>
                                                    ) : null}
                                                </div>
                                                <div>
                                                    <label htmlFor="lastName">Last Name * :</label>
                                                    <input
                                                        id="lastName"
                                                        name="lastName"
                                                        type="text"
                                                        onChange={handleChange}
                                                        onBlur={handleBlur}
                                                        value={values.lastName}
                                                        className="form-control"
                                                    />
                                                    {touched.lastName && errors.lastName ? (
                                                        <div className="text-danger">{errors.lastName}
                                                        </div>
                                                    ) : null}
                                                </div>
                                            </div>
                                            <div role="tabpanel" className="tab-pane fade" id="tab_content2" aria-labelledby="address-tab">

                                                <div>
                                                    <label htmlFor="number">Number :</label>
                                                    <input
                                                        id="number"
                                                        name="number"
                                                        type="text"
                                                        onChange={handleChange}
                                                        onBlur={handleBlur}
                                                        value={values.number}
                                                        className="form-control"
                                                    />
                                                    {touched.number && errors.number ? (
                                                        <div className="text-danger">{errors.number}

                                                        </div>
                                                    ) : null}
                                                </div>
                                                <div>
                                                    <label htmlFor="building">Building :</label>
                                                    <input
                                                        id="building"
                                                        name="building"
                                                        type="text"
                                                        onChange={handleChange}
                                                        onBlur={handleBlur}
                                                        value={values.building}
                                                        className="form-control"
                                                    />
                                                    {touched.building && errors.building ? (
                                                        <div className="text-danger">{errors.building}</div>
                                                    ) : null}
                                                </div>
                                                <div>
                                                    <label htmlFor="district">District Name :</label>
                                                    <input
                                                        id="district"
                                                        name="district"
                                                        type="text"
                                                        onChange={handleChange}
                                                        onBlur={handleBlur}
                                                        value={values.district}
                                                        className="form-control"
                                                    />
                                                    {touched.district && errors.district ? (
                                                        <div className="text-danger">{errors.district}</div>
                                                    ) : null}
                                                </div>
                                                <div>
                                                    <label htmlFor="city">city Name :</label>
                                                    <input
                                                        id="city"
                                                        name="city"
                                                        type="text"
                                                        onChange={handleChange}
                                                        onBlur={handleBlur}
                                                        value={values.city}
                                                        className="form-control"
                                                    />
                                                    {touched.city && errors.city ? (
                                                        <div className="text-danger">{errors.city}</div>
                                                    ) : null}
                                                </div>
                                                <div>
                                                    <label htmlFor="state">State Name :</label>
                                                    <input
                                                        id="state"
                                                        name="state"
                                                        type="text"
                                                        onChange={handleChange}
                                                        onBlur={handleBlur}
                                                        value={values.state}
                                                        className="form-control"
                                                    />
                                                    {touched.state && errors.state ? (
                                                        <div className="text-danger">{errors.state}</div>
                                                    ) : null}
                                                </div>
                                                <div>
                                                    <label htmlFor="zipCode">zipCode :</label>
                                                    <input
                                                        id="zipCode"
                                                        name="zipCode"
                                                        type="text"
                                                        onChange={handleChange}
                                                        onBlur={handleBlur}
                                                        value={values.zipCode}
                                                        className="form-control"
                                                    />
                                                    {touched.zipCode && errors.zipCode ? (
                                                        <div className="text-danger">{errors.zipCode}</div>
                                                    ) : null}
                                                </div>
                                                <div>
                                                    <label htmlFor="businessName">Country Name :</label>
                                                    <input
                                                        id="country"
                                                        name="country"
                                                        type="text"
                                                        onChange={handleChange}
                                                        onBlur={handleBlur}
                                                        value={values.country}
                                                        className="form-control"
                                                    />
                                                    {touched.country && errors.country ? (
                                                        <div className="text-danger">{errors.country}</div>
                                                    ) : null}
                                                </div>

                                            </div>
                                            <div role="tabpanel" className="tab-pane fade" id="tab_content3" aria-labelledby="operation-tab">
                                                <div>
                                                    <label htmlFor="openingTime">Opening Time :</label>
                                                    <input
                                                        id="openingTime"
                                                        name="openingTime"
                                                        type="openingTime"
                                                        onChange={handleChange}
                                                        onBlur={handleBlur}
                                                        value={values.openingTime}
                                                        className="form-control"
                                                    />
                                                    {touched.openingTime && errors.openingTime ? (
                                                        <div className="text-danger">{errors.openingTime}</div>
                                                    ) : null}
                                                </div>
                                                <div>
                                                    <label htmlFor="operatingHours">Operating Hours :</label>
                                                    <input
                                                        id="operatingHours"
                                                        name="operatingHours"
                                                        type="operatingHours"
                                                        onChange={handleChange}
                                                        onBlur={handleBlur}
                                                        value={values.operatingHours}
                                                        className="form-control"
                                                    />
                                                    {touched.operatingHours && errors.operatingHours ? (
                                                        <div className="text-danger">{errors.operatingHours}</div>
                                                    ) : null}
                                                </div>
                                                <div>
                                                    <label htmlFor="estimatedDeliveryTime">Estemated Delivery Time :</label>
                                                    <input
                                                        id="estimatedDeliveryTime"
                                                        name="estimatedDeliveryTime"
                                                        type="estimatedDeliveryTime"
                                                        onChange={handleChange}
                                                        onBlur={handleBlur}
                                                        value={values.estimatedDeliveryTime}
                                                        className="form-control"
                                                    />
                                                    {touched.estimatedDeliveryTime && errors.estimatedDeliveryTime ? (
                                                        <div className="text-danger">{errors.estimatedDeliveryTime}</div>
                                                    ) : null}
                                                </div>
                                                <div>
                                                    <label htmlFor="minimumOrder">Minimum Order :</label>
                                                    <input
                                                        id="minimumOrder"
                                                        name="minimumOrder"
                                                        type="minimumOrder"
                                                        onChange={handleChange}
                                                        onBlur={handleBlur}
                                                        value={values.minimumOrder}
                                                        className="form-control"
                                                    />
                                                    {touched.minimumOrder && errors.minimumOrder ? (
                                                        <div className="text-danger">{errors.minimumOrder}</div>
                                                    ) : null}
                                                </div>
                                            </div>
                                            <div role="tabpanel" className="tab-pane fade" id="tab_content4" aria-labelledby="upload-tab">
                                                <input name="file" id="file" type="file" title="Image" className="form-control" value="" onChange={(event) =>{
                                                    setFieldValue('File',event.currentTarget.files[0]);
                                                }}/>
                                            </div>
                                        </div>
                                        <br/>
                                        <br />
                                        <button type="submit" className="btn btn-success pull-right" disabled={isValid ? false : true}>Submit</button>
                                    </div>
                                </Form>
                            )}
                        </Formik>
                    </div>
                </div>
            </div>
        </div >
    )
}

