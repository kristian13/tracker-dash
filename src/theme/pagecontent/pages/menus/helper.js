import React from 'react';

export const MenuList = (props) => {
    
    return (
        <tr>
            <td>
                {props.name}<br/>
            </td>   
            <td>
                {props.description}
            </td>
            <td>
                {props.isActive ? (<span className="label label-success">Active</span>) : (<span className="label label-danger">In-active</span>)}
            </td>
            <td>
                <button onClick={props.edit.bind(this)} action="edit" className="buttonPrevious btn btn-xs btn-primary">Edit</button>  
            </td>
        </tr>
    )
}

