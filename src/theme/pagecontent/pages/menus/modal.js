import React from 'react';
import { useFormik } from 'formik';
import { WithContext as ReactTags } from 'react-tag-input';
import axios from 'axios';
import * as Yup from 'yup';


const KeyCodes = {comma: 188,enter: 13};
const delimiters = [KeyCodes.comma, KeyCodes.enter];

export const AddModal = (props) => {
    // let checked = props.modalData.isActive ? 'checked' : ''; 
    // let isChecked = props.modalData.isActive ? 'true' : 'false';  
   
    const formik = useFormik({
        // load data init 
        initialValues: {
            name:'',
            slug:'',

            description: ''
        },
        
        // validation filter
        validationSchema: Yup.object({
            name: Yup.string()
              .required('Required'),
            description: Yup.string()
              .required('Required'),
        }),

        // submit function
        onSubmit: values => {
            
            let menu = JSON.stringify({
                "menu": {
                    "restaurantId": props.tags[0]._id,
                    "name": values.name,
                    "description": values.description
                }
            });

            axios({
                url    : 'menus',
                method : 'POST',
                data   : menu
            }).then( result => {
                console.log(result);
                // if(result.)
                // console.log(result);
            }).catch(error => {
                props.badRequest(error);
            });
           
        },

    });

    // let isActive = props.modalData.isActive ? 'checked' : '';

    return (
        <div className="modal " style={{display:props.modalDisplay}}>
            <div className=" modal-dialog modal-lg">
                <div className="modal-content">
                    <div className="modal-header">
                        <span className="close" onClick={props.closeModal}>&times;</span>
                        <h2>Add Menu</h2>
                    </div>
                    <div className="modal-body">
                        <form onSubmit={formik.handleSubmit}>
                            {/* <div>
                                <div className="checkbox pull-right">
                                    <label className="">
                                        <div className={`icheckbox_flat-green ${isActive}`} onClick={props.SetActive} style={{Position: 'relative'}}></div> Is Active
                                    </label>
                                </div>
                            </div> */}
                            <div>
                                <br/>
                                <label htmlFor="Name">Restaurant Name:</label>
                                <ReactTags
                                    inputFieldPosition="bottom"
                                    tags={props.tags}
                                    suggestions={props.suggestions}
                                    handleDelete={props.handleDelete}
                                    handleAddition={props.handleAddition}
                                    handleTagClick={props.handleTagClick}
                                    delimiters={delimiters}
                                    placeholder="Restaurant Name"
                                />
                            </div>
                            <div>
                                <label htmlFor="Name">Name * :</label>
                                <input
                                    id="name"
                                    name="name"
                                    type="text"
                                    onChange={formik.handleChange}
                                    onBlur={formik.handleBlur}
                                    value={formik.values.name}
                                    className="form-control"
                                />
                                {formik.touched.name && formik.errors.name ? (
                                    <div className="text-danger">{formik.errors.name}</div>
                                ) : null}   
                            </div>
                            <div>
                                <label htmlFor="Description">Description * :</label>
                                <input
                                    id="description"
                                    name="description"
                                    type="text"
                                    onChange={formik.handleChange}
                                    onBlur={formik.handleBlur}
                                    value={formik.values.description}
                                    className="form-control"
                                />
                                {formik.touched.description && formik.errors.description ? (
                                    <div className="text-danger">{formik.errors.description}</div>
                                ) : null}     
                            </div>
                            <br/>
                            <button type="submit" className="btn btn-success btn-sm pull-right">Submit</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    )
}

// Make sure state data is set before calling the modal
// if not the initialValue will be null
export const EditModal = (props) => {
    // let checked = props.modalData.isActive ? 'checked' : ''; 
    // let isChecked = props.modalData.isActive ? 'true' : 'false';  
   
    const formik = useFormik({
        // load data init 
        initialValues: {...props.modalData},
        
        // validation filter
        validationSchema: Yup.object({
            //user
            userName: Yup.string()
                .min(1, 'Must be 1 characters or less'),
            emailAddress: Yup.string()
                .min(1, 'Must be 1 characters or less'),
            firstName: Yup.string()
                .min(1, 'Must be 1 characters or less'),
            lastName: Yup.string()
                .min(1, 'Must be 1 characters or less'),
            mobileNumber: Yup.string()
                .min(1, 'Must be 1 characters or less'),
            // missing
            // deviceId:
            // platform:
            // userType:
            // isUpdated
            
            //Address
            number: Yup.string()
                .min(1, 'Must be 1 characters or less'),
            building: Yup.string()
                .min(1, 'Must be 1 characters or less'),
            street: Yup.string()
                .min(1, 'Must be 1 characters or less'),
            district: Yup.string()
                .min(1, 'Must be 1 characters or less'),
            city: Yup.string()
                .min(1, 'Must be 1 characters or less'),
            state: Yup.string()
                .min(1, 'Must be 1 characters or less'),
            zipCode: Yup.string()
                .min(1, 'Must be 1 characters or less'),
            country: Yup.string()
                .min(1, 'Must be 1 characters or less'),
            type: Yup.string()
                .min(1, 'Must be 1 characters or less'),
            isPrimary: Yup.string()
                .min(1, 'Must be 1 characters or less'),
            isNew: Yup.string()
                .min(1, 'Must be 1 characters or less'),

            //Secondary address
            

        }),

        // submit function
        onSubmit: values => {

            let data = JSON.stringify({
                "menu": {
                    "restaurantId": props.tags[0]._id,
                    "name": values.name,
                    "description": values.description
                }
            });

            axios({
                url : 'menus/'+props.modalData._id,
                method : 'PUT',
                data: data
            }).then( result => {
                let data = result.data;          
                if(data.status === 200){
                    let alter = {...data.data,_id:props.modalData._id};
                    props.menu_update(alter);
                    props.closeModal();
                }
            }).catch(error => {
                props.badRequest(error);
            });
        }

    });

    let isActive = props.modalData.isActive ? 'checked' : '';

    return (
        <div className="modal " style={{display:props.modalDisplay}}>
            <div className=" modal-dialog modal-lg">
                <div className="modal-content">
                    <div className="modal-header">
                        <span className="close" onClick={props.closeModal}>&times;</span>
                        <h2>Edit Menu</h2>
                    </div>
                    <div className="modal-body">
                        <form onSubmit={formik.handleSubmit}>
                            {/* Tag input */}
                            <div>
                                <label htmlFor="Name">Restaurant Name:</label>
                                <ReactTags
                                    inputFieldPosition="bottom"
                                    tags={props.tags}
                                    suggestions={props.suggestions}
                                    handleDelete={props.handleDelete}
                                    handleAddition={props.handleAddition}
                                    handleTagClick={props.handleTagClick}
                                    delimiters={delimiters}
                                />
                            </div>
                            {/* CheckBox */}
                            <div>
                                <div className="checkbox pull-right">
                                    <label className="">
                                        <div className={`icheckbox_flat-green ${isActive}`} onClick={props.SetActive} style={{Position: 'relative'}}></div> Is Active
                                    </label>
                                </div>
                            </div>
                            <div>
                                <label htmlFor="Name">Name * :</label>
                                <input
                                    id="name"
                                    name="name"
                                    type="text"
                                    onChange={formik.handleChange}
                                    onBlur={formik.handleBlur}
                                    value={formik.values.name}
                                    className="form-control"
                                />
                                {formik.touched.name && formik.errors.name ? (
                                    <div className="text-danger">{formik.errors.name}</div>
                                ) : null}   
                            </div>
                            <div>
                                <label htmlFor="Description">Description * :</label>
                                <input
                                    id="description"
                                    name="description"
                                    type="text"
                                    onChange={formik.handleChange}
                                    onBlur={formik.handleBlur}
                                    value={formik.values.description}
                                    className="form-control"
                                />
                                {formik.touched.description && formik.errors.description ? (
                                    <div className="text-danger">{formik.errors.description}</div>
                                ) : null}     
                            </div>
                             <br/>
                            <button type="submit" className="btn btn-success btn-sm pull-right">Submit</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    )
}

