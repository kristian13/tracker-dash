import React from 'react';
import {connect} from 'react-redux';
import axios from 'axios';
import Pagination from "react-js-pagination";

import { MenuList } from './helper';
import { AddModal,EditModal } from './modal';

import './modal.css';

class Menus extends React.Component {

    _isMounted = false;
    constructor(props) {
        super();

        this.state = {
            modalDisplay : 'none',
            modalType : '',
            modalData : {},
            tags: [],
			suggestions: []
        }

        this.handleDelete = this.handleDelete.bind(this);
		this.handleAddition = this.handleAddition.bind(this);
		this.handleDrag = this.handleDrag.bind(this);
    }

    componentDidMount() {
        this._isMounted = true;
        axios({
            url:'/menus/all?sort=&order=&page=&limit=',
            method: 'GET'
        })
        .then((result) => {
            let menus = result.data;
            if(!this._isMounted){return false}
            if(menus.status === 200){
                this.props.menu_page_init(menus.data);
            }
        }).catch((e) =>{
            //console.log(e);
        });

        // restaurant tags
        axios({
            url:'/restaurants/all?sort=&order=&page=&limit',
            method: 'GET'
        })
        .then((result) => {
            let resto = result.data;
            if(resto.status === 200){
                let {restaurants} = resto.data;
                if(!this._isMounted){return false}
                let data = restaurants.filter(item => {
                    if(item.businessName === undefined) { return false;}
                    return true;
                }).map(item => {
                    return { id:item.businessName, _id:item._id, text:item.businessName }
                });
                this.setState({ suggestions:[...data] });
            }
        }).catch((e) =>{
            //console.log(e);
        });
    }

    componentWillUnmount() {
		this._isMounted = false;
	}

    handlePageChange = (pageNumber) => {
        axios({
            url : `menus/?sort=&order=&page=${pageNumber}&limit=${this.props.state.menus.pagination.currentLimit}`,
            method : 'GET'
        }).then((result) => {
            let data = result.data;
            if(data.status === 200){
                this.props.menu_page_update(data.data);
            }
        }).catch((e) => {
            console.log(e);
        });
    }

    ShowModal = (props, e) => {
        let action = e.target.getAttribute('action');
        
        if(action === 'edit'){
            this.setState({
                tags:[{
                    id:props.restaurant.businessName,
                    _id:props.restaurant._id,
                    text: props.restaurant.businessName
                }]
            })
        }

        this.setState({
            modalData: { ...this.state.modalData, ...props },
            modalDisplay: 'block',
            modalType: action
        });
    }

    closeModal = (obj) => {
        this.setState((prevState, props) => ({
            ...prevState,
            modalDisplay: 'none',
            modalType: '',
            modalData: {
                imageURL: '',
                isActive: false,
                name: '',
                description: '',
                slug: '',
            }
        }));
    }


    // FOR TAGS
		handleDelete(i,e) {
			//Disable Backspace
			if(e.keyCode === 8 ){return false;}

			const { tags } = this.state;
			this.setState({
				tags: tags.filter((tag, index) => index !== i),
			});
		}
	
		handleAddition(tag) {
            if(tag._id === undefined) {return false}
			this.setState(state => ({ tags: [tag] }));
		}
	
		handleDrag(tag, currPos, newPos) {
			const tags = [...this.state.tags];
			const newTags = tags.slice();
	
			newTags.splice(currPos, 1);
			newTags.splice(newPos, 0, tag);
	
			// re-render
			this.setState({ tags: newTags });
		}
	// END FOR TAGS


    render() {
        return(
            <div>
                <div>
                    {this.state.modalDisplay === 'block' && this.state.modalType === 'add' ?
                        <AddModal
                            modalDisplay={this.state.modalDisplay}
                            modalData={this.state.modalData}
                            menu_update={this.props.menu_update}
                            tags={this.state.tags}
                            suggestions={this.state.suggestions}
                            handleDelete={this.handleDelete}
                            handleAddition={this.handleAddition}
                            handleDrag={this.handleDrag}
                            closeModal={this.closeModal}
                        />
                    : null }

                    {this.state.modalDisplay === 'block' && this.state.modalType === 'edit' ?
                        <EditModal
                            modalDisplay={this.state.modalDisplay}
                            modalData={this.state.modalData}
                            menu_update={this.props.menu_update}
                            tags={this.state.tags}
                            suggestions={this.state.suggestions}
                            handleDelete={this.handleDelete}
                            handleAddition={this.handleAddition}
                            handleDrag={this.handleDrag}
                            closeModal={this.closeModal}
                        />
                    : null }
                </div>
                <div className="col-md-12 col-sm-6 col-xs-12">
                    <div className="page-title">
                        <div className="title_left">
                            <br />
                            <button type="button" className="btn btn-success btn-sm" onClick={this.ShowModal.bind(this, 'add')} action="add">
                                Add Menu
                            </button>
                            <div className="clearfix"></div>
                        </div>

                        <div className="title_right">
                            <div className="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                                <div className="input-group">
                                    <input type="text" className="form-control" placeholder="Search for..." />
                                    <span className="input-group-btn">
                                        <button className="btn btn-default" type="button">Go!</button>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="x_panel">
                        <div className="x_title">
                            <h2><i className="fa fa-bars"></i> Menus</h2>
                            <div className="nav navbar-right"></div>
                            <div className="clearfix"></div>
                        </div>
                        <div className="x_content">
                            <table className="table table-hover">
                                <thead>
                                    <tr>
                                        <th>Menu</th>
                                        <th>Description</th>
                                        <th>Active</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {this.props.state.menus.menus.length > 0 ? 
                                        this.props.state.menus.menus.map((customer,key) => (
                                            <MenuList
                                                key={key}
                                                {...customer}
                                                edit={this.ShowModal.bind(this, customer)} 
                                            />
                                        ))
                                    : null}
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

                <div className="col-lg-12">
                    <Pagination
                        innerClass="pagination pull-right" 
                        hideDisabled
                        activePage={this.props.state.menus.pagination.currentPage}
                        itemsCountPerPage={this.props.state.menus.pagination.currentLimit}
                        totalItemsCount={this.props.state.menus.pagination.recordCount}
                        pageRangeDisplayed={this.props.state.menus.pagination.currentLimit}
                        onChange={this.handlePageChange}
                    />
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        state: state,
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        menu_page_init:(data) => dispatch({type:'MENU_PAGE_INIT',payload:data}),
        menu_update:(data)    => dispatch({type:'MENU_UPDATE',payload:data}),
        resto_tags:(data)     => dispatch({type:'RESTAURANT_TAGS',payload:data})
    }
}

export default connect(mapStateToProps,mapDispatchToProps)(Menus);