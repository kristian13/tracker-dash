import React from 'react';
import {connect} from 'react-redux';
import axios from 'axios';
import Pagination from "react-js-pagination";

import { CustomerList } from './helper';
import { AddModal,EditModal } from './modal';

import './modal.css';

class Customers extends React.Component {

    _isMounted = false;
    
    constructor(props) {
        super();

        this.state = {
            modalDisplay : 'none',
            modalType : '',
            modalData : {}
        }
    }

    componentDidMount() {
        this._isMounted = true;
        axios({
            url:'/customers/all?sort=&order=&page=&limit=16',
            method: 'GET'
        })
        .then((result) => {
            let customer = result.data;
            if(!this._isMounted){return false}
            if(customer.status === 200){
                this.props.custormer_page_update(customer.data);
            }
        }).catch((e) =>{
            console.log(e);
        });
    }

    componentWillUnmount() {
		this._isMounted = false;
	}

    handlePageChange = (pageNumber) => {
        axios({
            url : `customers/all?sort=&order=&page=${pageNumber}&limit=${this.props.state.customers.pagination.currentLimit}`,
            method : 'GET'
        }).then((result) => {
            let data = result.data;
            this.props.custormer_page_update(data.data);
        }).catch((e) => {
            console.log(e);
        });
    }

    ShowModal = (props, e) => {
        let action = e.target.getAttribute('action');
      
        this.setState({
            modalData: { ...this.state.modalData, ...props },
            modalDisplay: 'block',
            modalType: action
        });
    }

    closeModal = (obj) => {
        this.setState((prevState, props) => ({
            ...prevState,
            modalDisplay: 'none',
            modalType: '',
            modalData: {
                imageURL: '',
                isActive: false,
                name: '',
                description: '',
                slug: '',
            }
        }));
    }

    render() {
        return(
            <div>
                <div>
                    {this.state.modalDisplay === 'block' && this.state.modalType === 'add' ?
                        <AddModal
                            modalDisplay={this.state.modalDisplay}
                            modalData={this.state.modalData}
                            closeModal={this.closeModal}
                        />
                    : null }

                    {this.state.modalDisplay === 'block' && this.state.modalType === 'edit' ?
                        <EditModal
                            modalDisplay={this.state.modalDisplay}
                            modalData={this.state.modalData}
                            closeModal={this.closeModal}
                        />
                    : null }
                </div>
                <div className="col-md-12 col-sm-6 col-xs-12">
                    <div className="page-title">
                        <div className="title_left">
                            <br />
                            <button type="button" className="btn btn-success btn-sm" onClick={this.ShowModal.bind(this, '')} action="add">
                                Add Customer
                            </button>
                            <div className="clearfix"></div>
                        </div>

                        <div className="title_right">
                            <div className="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                                <div className="input-group">
                                    <input type="text" className="form-control" placeholder="Search for..." />
                                    <span className="input-group-btn">
                                        <button className="btn btn-default" type="button">Go!</button>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="x_panel">
                        <div className="x_title">
                            <h2><i className="fa fa-bars"></i> Customer</h2>
                            <div className="nav navbar-right"></div>
                            <div className="clearfix"></div>
                        </div>
                        <div className="x_content">
                            <table className="table table-hover">
                                <thead>
                                    <tr>
                                        <th>Customers</th>
                                        <th>Address</th>
                                        <th>Active</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {this.props.state.customers.customers.length > 0 ? 
                                        this.props.state.customers.customers.map((customer,key) => (
                                            <CustomerList
                                                key={key}
                                                {...customer}
                                                edit={this.ShowModal.bind(this, customer)} 
                                            />
                                        ))
                                    : null}
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

                <div className="col-lg-12">
                    <Pagination
                        innerClass="pagination pull-right" 
                        hideDisabled
                        activePage={this.props.state.customers.pagination.currentPage}
                        itemsCountPerPage={this.props.state.customers.pagination.currentLimit}
                        totalItemsCount={this.props.state.customers.pagination.recordCount}
                        pageRangeDisplayed={this.props.state.customers.pagination.currentLimit}
                        onChange={this.handlePageChange}
                    />
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        state: state,
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        custormer_page_update:(data) => dispatch({type:'CUSTOMER_PAGE_UPDATE',payload:data}),
    }
}

export default connect(mapStateToProps,mapDispatchToProps)(Customers);