import React from 'react';
import { useFormik } from 'formik';
// import axios from 'axios';
import * as Yup from 'yup';


export const AddModal = (props) => {
    // let checked = props.modalData.isActive ? 'checked' : ''; 
    // let isChecked = props.modalData.isActive ? 'true' : 'false';  
   
    const formik = useFormik({
        // load data init 
        initialValues: {
            name:'',
            slug:'',

            description: ''
        },
        
        // validation filter
        validationSchema: Yup.object({
          name: Yup.string()
              .min(1, 'Must be 15 characters or less')
              .required('Required'),
          description: Yup.string()
              .min(1, 'Must be 15 characters or less')
              .required('Required'),
          slug: Yup.string()
              .min(1, 'Must be 15 characters or less')
              .required('Required'),
        }),

        // submit function
        onSubmit: values => {

            // let data = JSON.stringify({
            //     "user":{
            //         "_id": "5db9462448f4290f3e780f72",
            //         "userName":"trackcust",
            //         "emailAddress":"trackcust@trackresto.com",
            //         "firstName":"trackers",
            //         "lastName":"customer",
            //         "mobileNumber":"123456789",
            //         "deviceId":"dashboard",
            //         "platform":"dashboard",
            //         "userType":"customer",
            //         "isUpdated":true
            //     },
            //     "address": [
            //         {
            //         "_id": "5db9462448f4290f3e780f71",
            //         "number": "", 
            //         "building": "",
            //         "street": "",
            //         "district": "",
            //         "city":"Angeles City",
            //         "state":"Pampanga",
            //         "zipCode":"2009",
            //         "country":"Philippines",
            //         "type":"home",
            //         "isPrimary": true,
            //         "isNew": false
            //         },
            //         {
            //         "number": "", 
            //         "building": "",
            //         "street": "",
            //         "district": "",
            //         "city":"City of San Fernando",
            //         "state":"Pampanga",
            //         "zipCode":"2000",
            //         "country":"Philippines",
            //         "type":"additional",
            //         "isPrimary": false,
            //         "isNew": true
            //         }
            //     ]
            // });

            // axios({
            //     url : 'categories',
            //     method : 'POST',
            //     data: data
            // }).then( result => {
            //     console.log(result);
            // }).catch(error => {
            //     props.badRequest(error);
            // });

            console.log(props);
            return false;
        },

    });

    let isActive = props.modalData.isActive ? 'checked' : '';

    return (
        <div className="modal " style={{display:props.modalDisplay}}>
            <div className=" modal-dialog modal-lg">
                <div className="modal-content">
                    <div className="modal-header">
                        <span className="close" onClick={props.closeModal}>&times;</span>
                        <h2>Add Customer</h2>
                    </div>
                    <div className="modal-body">
                        <form onSubmit={formik.handleSubmit}>
                            <div>
                                <div className="checkbox pull-right">
                                    <label className="">
                                        <div className={`icheckbox_flat-green ${isActive}`} onClick={props.SetActive} style={{Position: 'relative'}}></div> Is Active
                                    </label>
                                </div>
                            </div>
                            <div>
                                <label htmlFor="Name">Name * :</label>
                                <input
                                    id="name"
                                    name="name"
                                    type="text"
                                    onChange={formik.handleChange}
                                    onBlur={formik.handleBlur}
                                    value={formik.values.name}
                                    className="form-control"
                                />
                                {formik.touched.name && formik.errors.name ? (
                                    <div className="text-danger">{formik.errors.name}</div>
                                ) : null}   
                            </div>
                            <div>
                                <label htmlFor="Description">Description * :</label>
                                <input
                                    id="description"
                                    name="description"
                                    type="text"
                                    onChange={formik.handleChange}
                                    onBlur={formik.handleBlur}
                                    value={formik.values.description}
                                    className="form-control"
                                />
                                {formik.touched.description && formik.errors.description ? (
                                    <div className="text-danger">{formik.errors.description}</div>
                                ) : null}     
                            </div>
                            <div>
                                <label htmlFor="Slug">Slug * :</label>
                                <input
                                    id="slug"
                                    name="slug"
                                    type="slug"
                                    onChange={formik.handleChange}
                                    onBlur={formik.handleBlur}
                                    value={formik.values.slug}
                                    className="form-control"
                                />
                                {formik.touched.slug && formik.errors.slug ? (
                                    <div className="text-danger">{formik.errors.slug}</div>
                                ) : null}    
                            </div>
                            <br/>
                            <button type="submit" className="btn btn-success btn-sm pull-right">Submit</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    )
}

// Make sure state data is set before calling the modal
// if not the initialValue will be null
export const EditModal = (props) => {
    // let checked = props.modalData.isActive ? 'checked' : ''; 
    // let isChecked = props.modalData.isActive ? 'true' : 'false';  
   
    const formik = useFormik({
        // load data init 
        initialValues: {...props.modalData},
        
        // validation filter
        validationSchema: Yup.object({
            //user
            userName: Yup.string()
                .min(1, 'Must be 1 characters or less'),
            emailAddress: Yup.string()
                .min(1, 'Must be 1 characters or less'),
            firstName: Yup.string()
                .min(1, 'Must be 1 characters or less'),
            lastName: Yup.string()
                .min(1, 'Must be 1 characters or less'),
            mobileNumber: Yup.string()
                .min(1, 'Must be 1 characters or less'),
            // missing
            // deviceId:
            // platform:
            // userType:
            // isUpdated
            
            //Address
            number: Yup.string()
                .min(1, 'Must be 1 characters or less'),
            building: Yup.string()
                .min(1, 'Must be 1 characters or less'),
            street: Yup.string()
                .min(1, 'Must be 1 characters or less'),
            district: Yup.string()
                .min(1, 'Must be 1 characters or less'),
            city: Yup.string()
                .min(1, 'Must be 1 characters or less'),
            state: Yup.string()
                .min(1, 'Must be 1 characters or less'),
            zipCode: Yup.string()
                .min(1, 'Must be 1 characters or less'),
            country: Yup.string()
                .min(1, 'Must be 1 characters or less'),
            type: Yup.string()
                .min(1, 'Must be 1 characters or less'),
            isPrimary: Yup.string()
                .min(1, 'Must be 1 characters or less'),
            isNew: Yup.string()
                .min(1, 'Must be 1 characters or less'),

            //Secondary address
            

        }),

        // submit function
        onSubmit: values => {

            // let data = JSON.stringify({
            //     "user":{
            //         "_id": "5db9462448f4290f3e780f72",
            //         "userName":"trackcust",
            //         "emailAddress":"trackcust@trackresto.com",
            //         "firstName":"trackers",
            //         "lastName":"customer",
            //         "mobileNumber":"123456789",
            //         "deviceId":"dashboard",
            //         "platform":"dashboard",
            //         "userType":"customer",
            //         "isUpdated":true
            //     },
            //     "address": [
            //         {
            //         "_id": "5db9462448f4290f3e780f71",
            //         "number": "", 
            //         "building": "",
            //         "street": "",
            //         "district": "",
            //         "city":"Angeles City",
            //         "state":"Pampanga",
            //         "zipCode":"2009",
            //         "country":"Philippines",
            //         "type":"home",
            //         "isPrimary": true,
            //         "isNew": false
            //         },
            //         {
            //         "number": "", 
            //         "building": "",
            //         "street": "",
            //         "district": "",
            //         "city":"City of San Fernando",
            //         "state":"Pampanga",
            //         "zipCode":"2000",
            //         "country":"Philippines",
            //         "type":"additional",
            //         "isPrimary": false,
            //         "isNew": true
            //         }
            //     ]
            // });

            // axios({
            //     url : 'categories',
            //     method : 'POST',
            //     data: data
            // }).then( result => {
            //     console.log(result);
            // }).catch(error => {
            //     props.badRequest(error);
            // });

            console.log(props);
            return false;

            // let data = JSON.stringify({
            //     'name':values.name,
            //     'slug':values.slug,
            //     'description':values.description,
            //     'isActive' : props.modalData.isActive
            // });

            // axios({
            //     url : 'categories/'+props.modalData._id,
            //     method : 'PUT',
            //     data: data
            // }).then( result => {
            //     if(result.data.status === 200){
            //         props.UpdateState(result.data);
            //     }
            // }).catch(error => {
            //     props.badRequest(error);
            // });
        }

    });

    let isActive = props.modalData.isActive ? 'checked' : '';

    return (
        <div className="modal " style={{display:props.modalDisplay}}>
            <div className=" modal-dialog modal-lg">
                <div className="modal-content">
                    <div className="modal-header">
                        <span className="close" onClick={props.closeModal}>&times;</span>
                        <h2>Edit Customer</h2>
                    </div>
                    <div className="modal-body">
                        <form onSubmit={formik.handleSubmit}>
                            <div>
                                <div className="checkbox pull-right">
                                    <label className="">
                                        <div className={`icheckbox_flat-green ${isActive}`} onClick={props.SetActive} style={{Position: 'relative'}}></div> Is Active
                                    </label>
                                </div>
                            </div>
                            <div>
                                <label htmlFor="Name">Name * :</label>
                                <input
                                    id="name"
                                    name="name"
                                    type="text"
                                    onChange={formik.handleChange}
                                    onBlur={formik.handleBlur}
                                    value={formik.values.name}
                                    className="form-control"
                                />
                                {formik.touched.name && formik.errors.name ? (
                                    <div className="text-danger">{formik.errors.name}</div>
                                ) : null}   
                            </div>
                            <div>
                                <label htmlFor="Description">Description * :</label>
                                <input
                                    id="description"
                                    name="description"
                                    type="text"
                                    onChange={formik.handleChange}
                                    onBlur={formik.handleBlur}
                                    value={formik.values.description}
                                    className="form-control"
                                />
                                {formik.touched.description && formik.errors.description ? (
                                    <div className="text-danger">{formik.errors.description}</div>
                                ) : null}     
                            </div>
                            <div>
                                <label htmlFor="Slug">Slug * :</label>
                                <input
                                    id="slug"
                                    name="slug"
                                    type="slug"
                                    onChange={formik.handleChange}
                                    onBlur={formik.handleBlur}
                                    value={formik.values.slug}
                                    className="form-control"
                                />
                                {formik.touched.slug && formik.errors.slug ? (
                                    <div className="text-danger">{formik.errors.slug}</div>
                                ) : null}    
                            </div>
                             <br/>
                            <button type="submit" className="btn btn-success btn-sm pull-right">Submit</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    )
}

