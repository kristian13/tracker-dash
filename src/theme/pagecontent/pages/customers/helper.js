import React from 'react';

export const CustomerList = (props) => {
    return (
        <tr>
            <td>
                Name : {props.firstName} {props.lastName} <br/>
                Email : {props.user.emailAddress} <br/>
                Username : {props.user.userName}
            </td>   
            <td>
                {props.primaryAddress ? (
                    <ul className="list-unstyled">
                        <li><i className=""></i>{props.primaryAddress.number ? props.primaryAddress.number : null}</li>
                        <li><i className=""></i>{props.primaryAddress.building ? props.primaryAddress.building : null}</li>
                        <li><i className=""></i>{props.primaryAddress.street ? props.primaryAddress.street : null}</li>
                        <li><i className=""></i>{props.primaryAddress.district ? props.primaryAddress.district : null}</li>
                        <li><i className=""></i>{props.primaryAddress.city ? props.primaryAddress.city : null}</li>
                        <li><i className=""></i>{props.primaryAddress.state ? props.primaryAddress.state : null}</li>
                        <li><i className=""></i>{props.primaryAddress.country ? props.primaryAddress.country : null}</li>
                    </ul>
                ) : null}
            </td>
            <td>
                {props.isActive ? (<span className="label label-success">Active</span>) : (<span className="label label-danger">In-active</span>)}
            </td>
            <td>
                <button onClick={props.edit.bind(this)} action="edit" className="buttonPrevious btn btn-xs btn-primary">Edit</button>  
            </td>
        </tr>
    )
}

