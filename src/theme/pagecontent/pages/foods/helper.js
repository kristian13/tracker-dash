import React from 'react';

export const FoodList = (props) => {
    return (
        <tr>
            <td>{props.name}</td>
            <td>{props.shortDescription}</td>
            <td>{props.isActive ? (<span className="label label-success">Active</span>) : (<span className="label label-danger">In-active</span>)}</td>
            <td>{props.price}</td>
            <td>{props.restaurant.businessName}</td>
            <td>{props.foodHoursStart}</td>
            <td>{props.foodHoursEnd}</td>
            <td>
                <button onClick={props.edit.bind(this)} action="edit" className="buttonPrevious btn btn-xs btn-primary">Edit</button>  
            </td>
        </tr>
    )
}




