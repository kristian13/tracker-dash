import React from 'react';
import { Formik } from 'formik';
// import axios from 'axios';
import * as Yup from 'yup';


export const AddModal = (props) => {
    
    let initValue = {
        restaurantId : '',
        name: '',
        price: '',
        shortDescription: '',
        description: '',
        minimumQuantity: '',
        maximumQuantity: '',
        prepTime: '',
        foodHoursStart: '',
        foodHoursEnd: '',
        isAvailable: true
    }

    let Schema = Yup.object({
        //user
        name: Yup.string()
            .required("Required")
            .min(1, 'Must be 1 characters or less'),
        price: Yup.string()
            .required("Required")
            .min(1, 'Must be 1 characters or less'),
        shortDescription: Yup.string()
            .required("Required")
            .min(1, 'Must be 1 characters or less'),
        description: Yup.string()
            .required("Required")
            .min(1, 'Must be 1 characters or less'),
        minimumQuantity: Yup.string()
            .required("Required")
            .min(1, 'Must be 1 characters or less'),
        maximumQuantity: Yup.string()
            .required("Required")
            .min(1, 'Must be 1 characters or less'),
        prepTime: Yup.string()
            .required("Required")
            .min(1, 'Must be 1 characters or less'),
        foodHoursStart: Yup.string()
            .required("Required")
            .min(1, 'Must be 1 characters or less'),
        foodHoursEnd: Yup.string()
            .required("Required")
            .min(1, 'Must be 1 characters or less'),
        isAvailable: Yup.string()
            .min(1, 'Must be 1 characters or less')
    });
    

    let isActive = props.modalData.isActive ? 'checked' : '';

    return (
        <div className="modal " style={{display:props.modalDisplay}}>
            <div className=" modal-dialog modal-lg">
                <div className="modal-content">
                    <div className="modal-header">
                        <span className="close" onClick={props.closeModal}>&times;</span>
                        <h2>Edit Customer</h2>
                    </div>
                    <div className="modal-body">
                        <Formik
                            initialValues={initValue}
                            validationSchema={Schema}
                            onSubmit={(values) => {

                            }}
                        >
                            
                            {({ values, errors, touched, handleBlur, handleChange, handleSubmit}) => 
                                <form onSubmit={handleSubmit}>
                                    <div>
                                        <div className="checkbox pull-right hide">
                                            <label className="">
                                                <div className={`icheckbox_flat-green ${isActive}`} onClick={props.SetActive} style={{Position: 'relative'}}></div> Is Active
                                            </label>
                                        </div>
                                    </div>
                                    <div>
                                    <label htmlFor="Name">Name * :</label>
                                    <input
                                        id="name"
                                        name="name"
                                        type="text"
                                        onChange={handleChange}
                                        onBlur={handleBlur}
                                        value={values.name}
                                        className="form-control"
                                    />
                                    {touched.name && errors.name ? (
                                        <div className="text-danger">{errors.name}</div>
                                    ) : null}   
                                    </div>
                                    <div>
                                        <label htmlFor="Description">Description * :</label>
                                        <input
                                            id="description"
                                            name="description"
                                            type="text"
                                            onChange={handleChange}
                                            onBlur={handleBlur}
                                            value={values.description}
                                            className="form-control"
                                        />
                                        {touched.description && errors.description ? (
                                            <div className="text-danger">{errors.description}</div>
                                        ) : null}     
                                    </div>
                                    <div>
                                        <label htmlFor="shortDescription">Short Description * :</label>
                                        <input
                                            id="shortDescription"
                                            name="shortDescription"
                                            type="text"
                                            onChange={handleChange}
                                            onBlur={handleBlur}
                                            value={values.shortDescription}
                                            className="form-control"
                                        />
                                        {touched.shortDescription && errors.shortDescription ? (
                                            <div className="text-danger">{errors.shortDescription}</div>
                                        ) : null}    
                                    </div>
                                    <div>
                                        <label htmlFor="price">Minimum Quantity * :</label>
                                        <input
                                            id="minimumQuantity"
                                            name="minimumQuantity"
                                            type="text"
                                            onChange={handleChange}
                                            onBlur={handleBlur}
                                            value={values.price}
                                            className="form-control"
                                        />
                                        {touched.price && errors.price ? (
                                            <div className="text-danger">{errors.price}</div>
                                        ) : null}    
                                    </div>
                                    <div>
                                        <label htmlFor="price">Maximum Quantity * :</label>
                                        <input
                                            id="maximumQuantity"
                                            name="maximumQuantity"
                                            type="text"
                                            onChange={handleChange}
                                            onBlur={handleBlur}
                                            value={values.maximumQuantity}
                                            className="form-control"
                                        />
                                        {touched.maximumQuantity && errors.maximumQuantity ? (
                                            <div className="text-danger">{errors.maximumQuantity}</div>
                                        ) : null}    
                                    </div>
                                    <div>
                                        <label htmlFor="price">Preperation Time * :</label>
                                        <input
                                            id="prepTime"
                                            name="prepTime"
                                            type="text"
                                            onChange={handleChange}
                                            onBlur={handleBlur}
                                            value={values.prepTime}
                                            className="form-control"
                                        />
                                        {touched.prepTime && errors.prepTime ? (
                                            <div className="text-danger">{errors.prepTime}</div>
                                        ) : null}    
                                    </div>
                                    <div>
                                        <label htmlFor="price">Food Hours Start * :</label>
                                        <input
                                            id="foodHoursStart"
                                            name="foodHoursStart"
                                            type="text"
                                            onChange={handleChange}
                                            onBlur={handleBlur}
                                            value={values.foodHoursStart}
                                            className="form-control"
                                        />
                                        {touched.foodHoursStart && errors.foodHoursStart ? (
                                            <div className="text-danger">{errors.foodHoursStart}</div>
                                        ) : null}    
                                    </div>
                                    <div>
                                        <label htmlFor="price">food Hours End * :</label>
                                        <input
                                            id="foodHoursEnd"
                                            name="foodHoursEnd"
                                            type="text"
                                            onChange={handleChange}
                                            onBlur={handleBlur}
                                            value={values.foodHoursEnd}
                                            className="form-control"
                                        />
                                        {touched.foodHoursEnd && errors.foodHoursEnd ? (
                                            <div className="text-danger">{errors.foodHoursEnd}</div>
                                        ) : null}    
                                    </div>
                                    <br/>
                                    <button type="submit" className="btn btn-success btn-sm pull-right">Submit</button>
                                </form>
                            }
                        </Formik>
                    </div>
                </div>
            </div>
        </div>
    )
}

// Make sure state data is set before calling the modal
// if not the initialValue will be null
export const EditModal = (props) => {
        
    let initValue = {
        restaurantId : props.modalData.restaurant._id,
        name: props.modalData.name,
        price: props.modalData.price,
        shortDescription: props.modalData.shortDescription,
        description: props.modalData.description,
        minimumQuantity: props.modalData.minimumQuantity,
        maximumQuantity: props.modalData.maximumQuantity,
        prepTime: props.modalData.prepTime,
        foodHoursStart: props.modalData.foodHoursStart,
        foodHoursEnd: props.modalData.foodHoursEnd,
        isAvailable: props.modalData.isActive
    }

    let Schema = Yup.object({
        //user
        name: Yup.string()
            .required("Required")
            .min(1, 'Must be 1 characters or less'),
        price: Yup.string()
            .required("Required")
            .min(1, 'Must be 1 characters or less'),
        shortDescription: Yup.string()
            .required("Required")
            .min(1, 'Must be 1 characters or less'),
        description: Yup.string()
            .required("Required")
            .min(1, 'Must be 1 characters or less'),
        minimumQuantity: Yup.string()
            .required("Required")
            .min(1, 'Must be 1 characters or less'),
        maximumQuantity: Yup.string()
            .required("Required")
            .min(1, 'Must be 1 characters or less'),
        prepTime: Yup.string()
            .required("Required")
            .min(1, 'Must be 1 characters or less'),
        foodHoursStart: Yup.string()
            .required("Required")
            .min(1, 'Must be 1 characters or less'),
        foodHoursEnd: Yup.string()
            .required("Required")
            .min(1, 'Must be 1 characters or less'),
        isAvailable: Yup.string()
            .min(1, 'Must be 1 characters or less')
    });
    

    let isActive = props.modalData.isActive ? 'checked' : '';

    return (
        <div className="modal " style={{display:props.modalDisplay}}>
            <div className=" modal-dialog modal-lg">
                <div className="modal-content">
                    <div className="modal-header">
                        <span className="close" onClick={props.closeModal}>&times;</span>
                        <h2>Edit Customer</h2>
                    </div>
                    <div className="modal-body">
                        <Formik
                            initialValues={initValue}
                            validationSchema={Schema}
                            onSubmit={(values) => {

                            }}
                        >
                            
                            {({ values, errors, touched, handleBlur, handleChange, handleSubmit}) => 
                                <form onSubmit={handleSubmit}>
                                    <div>
                                        <div className="checkbox pull-right">
                                            <label className="">
                                                <div className={`icheckbox_flat-green ${isActive}`} onClick={props.SetActive} style={{Position: 'relative'}}></div> Is Active
                                            </label>
                                        </div>
                                    </div>
                                    <div>
                                    <label htmlFor="Name">Name * :</label>
                                    <input
                                        id="name"
                                        name="name"
                                        type="text"
                                        onChange={handleChange}
                                        onBlur={handleBlur}
                                        value={values.name}
                                        className="form-control"
                                    />
                                    {touched.name && errors.name ? (
                                        <div className="text-danger">{errors.name}</div>
                                    ) : null}   
                                    </div>
                                    <div>
                                        <label htmlFor="Description">Description * :</label>
                                        <input
                                            id="description"
                                            name="description"
                                            type="text"
                                            onChange={handleChange}
                                            onBlur={handleBlur}
                                            value={values.description}
                                            className="form-control"
                                        />
                                        {touched.description && errors.description ? (
                                            <div className="text-danger">{errors.description}</div>
                                        ) : null}     
                                    </div>
                                    <div>
                                        <label htmlFor="shortDescription">Short Description * :</label>
                                        <input
                                            id="shortDescription"
                                            name="shortDescription"
                                            type="text"
                                            onChange={handleChange}
                                            onBlur={handleBlur}
                                            value={values.shortDescription}
                                            className="form-control"
                                        />
                                        {touched.shortDescription && errors.shortDescription ? (
                                            <div className="text-danger">{errors.shortDescription}</div>
                                        ) : null}    
                                    </div>
                                    <div>
                                        <label htmlFor="price">Minimum Quantity * :</label>
                                        <input
                                            id="minimumQuantity"
                                            name="minimumQuantity"
                                            type="text"
                                            onChange={handleChange}
                                            onBlur={handleBlur}
                                            value={values.price}
                                            className="form-control"
                                        />
                                        {touched.price && errors.price ? (
                                            <div className="text-danger">{errors.price}</div>
                                        ) : null}    
                                    </div>
                                    <div>
                                        <label htmlFor="price">Maximum Quantity * :</label>
                                        <input
                                            id="maximumQuantity"
                                            name="maximumQuantity"
                                            type="text"
                                            onChange={handleChange}
                                            onBlur={handleBlur}
                                            value={values.maximumQuantity}
                                            className="form-control"
                                        />
                                        {touched.maximumQuantity && errors.maximumQuantity ? (
                                            <div className="text-danger">{errors.maximumQuantity}</div>
                                        ) : null}    
                                    </div>
                                    <div>
                                        <label htmlFor="price">Preperation Time * :</label>
                                        <input
                                            id="prepTime"
                                            name="prepTime"
                                            type="text"
                                            onChange={handleChange}
                                            onBlur={handleBlur}
                                            value={values.prepTime}
                                            className="form-control"
                                        />
                                        {touched.prepTime && errors.prepTime ? (
                                            <div className="text-danger">{errors.prepTime}</div>
                                        ) : null}    
                                    </div>
                                    <div>
                                        <label htmlFor="price">Food Hours Start * :</label>
                                        <input
                                            id="foodHoursStart"
                                            name="foodHoursStart"
                                            type="text"
                                            onChange={handleChange}
                                            onBlur={handleBlur}
                                            value={values.foodHoursStart}
                                            className="form-control"
                                        />
                                        {touched.foodHoursStart && errors.foodHoursStart ? (
                                            <div className="text-danger">{errors.foodHoursStart}</div>
                                        ) : null}    
                                    </div>
                                    <div>
                                        <label htmlFor="price">food Hours End * :</label>
                                        <input
                                            id="foodHoursEnd"
                                            name="foodHoursEnd"
                                            type="text"
                                            onChange={handleChange}
                                            onBlur={handleBlur}
                                            value={values.foodHoursEnd}
                                            className="form-control"
                                        />
                                        {touched.foodHoursEnd && errors.foodHoursEnd ? (
                                            <div className="text-danger">{errors.foodHoursEnd}</div>
                                        ) : null}    
                                    </div>
                                    <br/>
                                    <button type="submit" className="btn btn-success btn-sm pull-right">Submit</button>
                                </form>
                            }
                        </Formik>
                    </div>
                </div>
            </div>
        </div>
    )
}

