import React from 'react';
import {connect} from 'react-redux';
import axios from 'axios';
import Pagination from "react-js-pagination";

import { FoodList } from './helper';
import { AddModal,EditModal } from './modal';

import './modal.css';

class Foods extends React.Component {

    _isMounted = false;

    constructor(props) {
        super();

        this.state = {
            modalDisplay : 'none',
            modalType : '',
            modalData : {}
        }
    }

    componentDidMount() {
        this._isMounted = true;

        axios({
            url:`foods/all?sort=&order=&page=&limit=${this.props.foods.pagination.currentLimit}`,
            method: 'GET'
            })
            .then((result) => {
                let foods = result.data;
                if(!this._isMounted){return false}
                if(foods.status === 200){
                    this.props.food_page_init(foods.data);
                }
            }).catch((e) =>{
                console.log(e.response);
            });
    }

    componentWillUnmount() {
		this._isMounted = false;
	}

    handlePageChange = (pageNumber) => {
        axios({
            url : `foods/?sort=&order=&page=${pageNumber}&limit=${this.props.state.customers.pagination.currentLimit}`,
            method : 'GET'
        }).then((result) => {
            let data = result.data;
            this.props.custormer_page_update(data.data);
        }).catch((e) => {
            console.log(e);
        });
    }

    SetActive = (event) => {
        this.setState((prevState) => ({
            modalData: {
                ...prevState.modalData,
                isActive: !this.state.modalData.isActive
            }
        }));
    }

    ShowModal = (props, e) => {
        let action = e.target.getAttribute('action');
      
        this.setState({
            modalData: { ...this.state.modalData, ...props },
            modalDisplay: 'block',
            modalType: action
        });
    }

    closeModal = (obj) => {
        this.setState((prevState, props) => ({
            ...prevState,
            modalDisplay: 'none',
            modalType: '',
            modalData: {
                imageURL: '',
                isActive: false,
                name: '',
                description: '',
                slug: '',
            }
        }));
    }

    render() {
        return(
            <div>
                <div>
                    {this.state.modalDisplay === 'block' && this.state.modalType === 'add' ?
                        <AddModal
                            modalDisplay={this.state.modalDisplay}
                            modalData={this.state.modalData}
                            closeModal={this.closeModal}
                        />
                    : null }

                    {this.state.modalDisplay === 'block' && this.state.modalType === 'edit' ?
                        <EditModal
                            modalDisplay={this.state.modalDisplay}
                            modalData={this.state.modalData}
                            SetActive={this.SetActive}
                            closeModal={this.closeModal}
                        />
                    : null }
                </div>
                <div className="col-md-12 col-sm-6 col-xs-12">
                    <div className="page-title">
                        <div className="title_left">
                            <br />
                            <button type="button" className="btn btn-success btn-sm" onClick={this.ShowModal.bind(this, '')} action="add">
                                Add Food
                            </button>
                            <div className="clearfix"></div>
                        </div>

                        <div className="title_right">
                            <div className="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                                <div className="input-group">
                                    <input type="text" className="form-control" placeholder="Search for..." />
                                    <span className="input-group-btn">
                                        <button className="btn btn-default" type="button">Go!</button>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="x_panel">
                        <div className="x_title">
                            <h2><i className="fa fa-bars"></i> Foods</h2>
                            <div className="nav navbar-right"></div>
                            <div className="clearfix"></div>
                        </div>
                        <div className="x_content">
                            <table className="table table-hover">
                                <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Description</th>
                                        <th>Active</th>
                                        <th>Price</th>
                                        <th>Restaurant</th>
                                        <th>Food Hours Start</th>
                                        <th>Food Hours End</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {this.props.foods.foods.length > 0 ? 
                                        this.props.foods.foods.map((food,key) => (
                                            <FoodList
                                                key={key}
                                                {...food}
                                                edit={this.ShowModal.bind(this, food)} 
                                            />
                                        ))
                                    : null}
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

                <div className="col-lg-12">
                    <Pagination
                        innerClass="pagination pull-right" 
                        hideDisabled
                        activePage={this.props.foods.pagination.currentPage}
                        itemsCountPerPage={this.props.foods.pagination.currentLimit}
                        totalItemsCount={this.props.foods.pagination.recordCount}
                        pageRangeDisplayed={this.props.foods.pagination.currentLimit}
                        onChange={this.handlePageChange}
                    />
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        state: state,
        foods: state.foods
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        food_page_init:(data)        => dispatch({type:'FOOD_PAGE_INIT',payload:data}),
        custormer_page_update:(data) => dispatch({type:'CUSTOMER_PAGE_UPDATE',payload:data}),
    }
}

export default connect(mapStateToProps,mapDispatchToProps)(Foods);