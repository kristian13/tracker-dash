import React from 'react';
import { connect } from 'react-redux';
import Pagination from "react-js-pagination";
import { CategoryList } from './helper';
import { EditModal, AddModal } from './modal';
import './modal.css';

import axios from 'axios';

class Categories extends React.Component {

    _isMounted = false;

    constructor(props) {
        super(props);
        
        this.state = {
            modalDisplay: 'none',
            modalType: '',
            modalData: {
                imageURL: '',
                isActive: false,
                name: '',
                description: '',
                slug: '',
            },
            upload : {}
        };

        this.handlePageChange = this.handlePageChange.bind(this);

    }

    componentDidMount() {
        this._isMounted = true;
        
        axios.get(`categories/?sort=&order=desc&page=&limit=${this.props.categories.pagination.currentLimit}`)
        .then((result) => {
            let data = result.data.data;
            if(!this._isMounted){return false}
            this.props.category_init(data);
        });
    }
    
    componentWillUnmount() {
		this._isMounted = false;
	}

    badRequest = (error) => {
        if (error.response !== undefined) {
            let e = error.response.data;
            if (e.status === 400) {
				if(e.data.error) {
					switch(e.data.error.message) {
						case 'jwt expired':
							alert('TOKEN EXPIRED LOGIN AGAIN...');
							this.props.setLogout();
						break;
						default:
						// code block
				  }
				}
            }
        }
	}
	
    ShowModal = (props, e) => {
        let action = e.target.getAttribute('action');
        this.setState({
            modalData: { ...this.state.modalData, ...props },
            modalDisplay: 'block',
            modalType: action
        });
    }

    SetActive = (event) => {
        this.setState((prevState) => ({
            modalData: {
                ...prevState.modalData,
                isActive: !this.state.modalData.isActive
            }
        }));
    }

    handlePageChange = (pageNumber) => {
        axios.get(`categories/?sort=&order=desc&page=${pageNumber}&limit=${this.props.categories.pagination.currentLimit}`)
        .then((result) => {
            let data = result.data.data;
            this.props.category_page_update(data);
        });
    }

    updateImage = (props) =>{
        let update = { ...this.props.categories };

        let setUpdate = update.categories.map(obj => (
            obj._id === props._id ? { ...obj, ...props } : obj
        ))
        
        this.props.category_state_update(setUpdate);
       
        this.closeModal();
    }

    UpdateState = (props,action) => {

        if(action === 'add'){
            let newData = [{...props},...this.props.categories.categories];
            this.props.category_state_update(newData);
          
        }

        if(action === 'edit'){
            let update = { ...this.props.categories };
            let setUpdate = update.categories.map(obj => (
                obj._id === props.data._id ? { ...obj, ...props.data } : obj
            ));

            this.props.category_state_update(setUpdate);
        }
        
        this.closeModal();
    }

    closeModal = (obj) => {
        this.setState((prevState, props) => ({
            ...prevState,
            modalDisplay: 'none',
            modalType: '',
            modalData: {
                imageURL: '',
                isActive: false,
                name: '',
                description: '',
                slug: '',
            }
        }));
    }

    render() {
        return (
            <div>
                {/* MODALS */}
                <div>
                    {/* check state modal before calling modal */}
                    {this.state.modalDisplay !== 'none' && this.state.modalType === 'edit' ? (
                        <EditModal
                            modalDisplay={this.state.modalDisplay}
                            modalData={this.state.modalData}
                            badRequest={this.badRequest}
                            SetActive={this.SetActive}
                            updateImage={this.updateImage}
                            UpdateState={this.UpdateState}
                            logOut={this.props.state.setLogout}
                            closeModal={this.closeModal} />
                    ) : null}

                    {this.state.modalDisplay !== 'none' && this.state.modalType === 'add' ? (
                        <AddModal
                            modalDisplay={this.state.modalDisplay}
                            modalData={this.state.modalData}
                            badRequest={this.badRequest}
                            SetActive={this.SetActive}
                            UpdateState={this.UpdateState}
                            logOut={this.props.state.setLogout}
                            closeModal={this.closeModal} />
                    ) : null}
                </div>
                <div className="col-md-12 col-sm-6 col-xs-12">
                    <div className="page-title">
                        <div className="title_left">
                            <br />
                            <button type="button" className="btn btn-success btn-sm" onClick={this.ShowModal.bind(this, '')} action="add">
                                Add Category
                            </button>
                            <div className="clearfix"></div>
                        </div>

                        <div className="title_right">
                            <div className="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                                <div className="input-group">
                                    <input type="text" className="form-control" placeholder="Search for..." />
                                    <span className="input-group-btn">
                                        <button className="btn btn-default" type="button">Go!</button>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="x_panel">
                        <div className="x_title">
                            <h2><i className="fa fa-bars"></i> Categories</h2>
                            <div className="nav navbar-right">
                            {/* <button type="button" className="btn btn-success btn-sm" onClick={this.ShowModal.bind(this,'')} action="add"> 
                                    Add Category 
                            </button> */}
                            </div>
                            {/* <ul className="nav navbar-right panel_toolbox ">
                                <li>
                                    <select>
                                        <option>Category</option>
                                        <option>Description</option>
                                    </select>
                                </li>
                                <li>
                                    <input type="text" />
                                </li>
                            </ul> */}
                            <div className="clearfix"></div>
                        </div>
                        <div className="x_content">
                            <table className="table table-hover">
                                <thead>
                                    <tr>
                                        {/* <th>#</th> */}
                                        <th>Image</th>
                                        <th>Description</th>
                                        <th>Active</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {this.props.categories.categories.length > 0 ? (
                                        this.props.categories.categories.map((category, index) =>
                                            <CategoryList
                                                key={index}
                                                {...category}
                                                edit={this.ShowModal.bind(this, category)}
                                            />
                                        )
                                    ) : null}
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
               
                <div className="col-lg-12">
                    <Pagination
                        innerClass="pagination pull-right" 
                        hideDisabled
                        activePage={this.props.categories.pagination.currentPage}
                        itemsCountPerPage={this.props.categories.pagination.currentLimit}
                        totalItemsCount={this.props.categories.pagination.recordCount}
                        pageRangeDisplayed={this.props.categories.pagination.currentLimit}
                        onChange={this.handlePageChange}
                    />
                </div>
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        state: state,
        categories: state.categories
    }
}

const mapDispatchToProps = dispatch => {
    return {
        category_init: (data) => dispatch({type:'CATEGORY_INIT',payload:data}),
        category_page_update: (data) => dispatch({type:'CATEGORY_PAGE_UPDATE',payload:data}),
        category_state_update: (data) => dispatch({type:'CATEGORY_STATE_UPDATE',payload:data}),
        badRequest: (data) => dispatch({ type: 'VALID_TOKEN_UPDATE', payload: data }),
        setLogout: (data) => dispatch({ type: 'AUTH_LOGOUT', payload: data })
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(Categories);