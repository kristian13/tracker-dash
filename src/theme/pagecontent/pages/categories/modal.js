import React from 'react';
import { useFormik, Formik , Form, Field  } from 'formik';
import axios from 'axios';
import * as Yup from 'yup';


export const AddModal = (props) => {
    // let checked = props.modalData.isActive ? 'checked' : ''; 
    // let isChecked = props.modalData.isActive ? 'true' : 'false';  
   
    const formik = useFormik({
        // load data init 
        initialValues: {
            name:'',
            slug:'',

            description: ''
        },
        
        // validation filter
        validationSchema: Yup.object({
          name: Yup.string()
              .min(1, 'Must be 15 characters or less')
              .required('Required'),
          description: Yup.string()
              .min(1, 'Must be 15 characters or less')
              .required('Required'),
          slug: Yup.string()
              .min(1, 'Must be 15 characters or less')
              .required('Required'),
        }),

        // submit function
        onSubmit: values => {

            let data = JSON.stringify({
                'name':values.name,
                'slug':values.slug,
                'description':values.description,
                'isActive' : props.modalData.isActive
            });

            axios({
                url : 'categories',
                method : 'POST',
                data: data
            }).then( result => {
                let data = result.data;
                if(data.status === 200){
                    props.UpdateState(data.data,'add');
                }
            }).catch(error => {
                props.badRequest(error);
            });
        },

    });

    let isActive = props.modalData.isActive ? 'checked' : '';

    return (
        <div className="modal " style={{display:props.modalDisplay}}>
            <div className=" modal-dialog modal-lg">
                <div className="modal-content">
                    <div className="modal-header">
                        <span className="close" onClick={props.closeModal}>&times;</span>
                        <h2>Add Category</h2>
                    </div>
                    <div className="modal-body">
                        <form onSubmit={formik.handleSubmit}>
                            <div>
                                <div className="checkbox pull-right">
                                    <label className="">
                                        <div className={`icheckbox_flat-green ${isActive}`} onClick={props.SetActive} style={{Position: 'relative'}}></div> Is Active
                                    </label>
                                </div>
                            </div>
                            <div>
                                <label htmlFor="Name">Name * :</label>
                                <input
                                    id="name"
                                    name="name"
                                    type="text"
                                    onChange={formik.handleChange}
                                    onBlur={formik.handleBlur}
                                    value={formik.values.name}
                                    className="form-control"
                                />
                                {formik.touched.name && formik.errors.name ? (
                                    <div className="text-danger">{formik.errors.name}</div>
                                ) : null}   
                            </div>
                            <div>
                                <label htmlFor="Description">Description * :</label>
                                <input
                                    id="description"
                                    name="description"
                                    type="text"
                                    onChange={formik.handleChange}
                                    onBlur={formik.handleBlur}
                                    value={formik.values.description}
                                    className="form-control"
                                />
                                {formik.touched.description && formik.errors.description ? (
                                    <div className="text-danger">{formik.errors.description}</div>
                                ) : null}     
                            </div>
                            <div>
                                <label htmlFor="Slug">Slug * :</label>
                                <input
                                    id="slug"
                                    name="slug"
                                    type="slug"
                                    onChange={formik.handleChange}
                                    onBlur={formik.handleBlur}
                                    value={formik.values.slug}
                                    className="form-control"
                                />
                                {formik.touched.slug && formik.errors.slug ? (
                                    <div className="text-danger">{formik.errors.slug}</div>
                                ) : null}    
                            </div>
                            <br/>
                            <button type="submit" className="btn btn-success btn-sm pull-right">Submit</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    )
}

// Make sure state data is set before calling the modal
// if not the initialValue will be null
export const EditModal = (props) => {
    // let checked = props.modalData.isActive ? 'checked' : ''; 
    // let isChecked = props.modalData.isActive ? 'true' : 'false';  
   
    const formik = useFormik({
        // load data init 
        initialValues: {...props.modalData},
        
        // validation filter
        validationSchema: Yup.object({
          name: Yup.string()
              .min(1, 'Must be 15 characters or less')
              .required('Required'),
          description: Yup.string()
              .min(1, 'Must be 15 characters or less')
              .required('Required'),
          slug: Yup.string()
              .min(1, 'Must be 15 characters or less')
              .required('Required'),
        }),

        // submit function
        onSubmit: values => {

            let data = JSON.stringify({
                'name':values.name,
                'slug':values.slug,
                'description':values.description,
                'isActive' : props.modalData.isActive
            });

            axios({
                url : 'categories/'+props.modalData._id,
                method : 'PUT',
                data: data
            }).then( result => {
                if(result.data.status === 200){
                    props.UpdateState(result.data,'edit');
                }
            }).catch(error => {
                props.badRequest(error);
            });
        },

    });

    let isActive = props.modalData.isActive ? 'checked' : '';

    return (
        <div className="modal " style={{display:props.modalDisplay}}>
            <div className=" modal-dialog modal-lg">
                <div className="modal-content">
                    <div className="modal-header">
                        <span className="close" onClick={props.closeModal}>&times;</span>
                        <h2>Edit Category</h2>
                    </div>
                    <div className="modal-body">
                        <ul id="myTab" className="nav nav-tabs bar_tabs" role="tablist">
                            <li role="presentation" className="active"><a href="#tab_content1" id="business-tab" role="tab" data-toggle="tab" aria-expanded="false">Category</a>
                            </li>
                            <li role="presentation" className=""><a href="#tab_content2" role="tab" id="address-tab" data-toggle="tab" aria-expanded="false">Upload Image</a>
                            </li>
                        </ul>
                        <div id="myTabContent" className="tab-content">
                            <div role="tabpanel" className="tab-pane active fade in" id="tab_content1" aria-labelledby="business-tab">
                                <form onSubmit={formik.handleSubmit}>
                                    <div>
                                        <div className="checkbox pull-right">
                                            <label className="">
                                                <div className={`icheckbox_flat-green ${isActive}`} onClick={props.SetActive} style={{Position: 'relative'}}></div> Is Active
                                            </label>
                                        </div>
                                    </div>
                                    <div>
                                        <label htmlFor="Name">Name * :</label>
                                        <input
                                            id="name"
                                            name="name"
                                            type="text"
                                            onChange={formik.handleChange}
                                            onBlur={formik.handleBlur}
                                            value={formik.values.name}
                                            className="form-control"
                                        />
                                        {formik.touched.name && formik.errors.name ? (
                                            <div className="text-danger">{formik.errors.name}</div>
                                        ) : null}   
                                    </div>
                                    <div>
                                        <label htmlFor="Description">Description * :</label>
                                        <input
                                            id="description"
                                            name="description"
                                            type="text"
                                            onChange={formik.handleChange}
                                            onBlur={formik.handleBlur}
                                            value={formik.values.description}
                                            className="form-control"
                                        />
                                        {formik.touched.description && formik.errors.description ? (
                                            <div className="text-danger">{formik.errors.description}</div>
                                        ) : null}     
                                    </div>
                                    <div>
                                        <label htmlFor="Slug">Slug * :</label>
                                        <input
                                            id="slug"
                                            name="slug"
                                            type="slug"
                                            onChange={formik.handleChange}
                                            onBlur={formik.handleBlur}
                                            value={formik.values.slug}
                                            className="form-control"
                                        />
                                        {formik.touched.slug && formik.errors.slug ? (
                                            <div className="text-danger">{formik.errors.slug}</div>
                                        ) : null}    
                                    </div>
                                    <br/>
                                    <button type="submit" className="btn btn-success btn-sm pull-right">Submit</button>
                                </form>
                            </div>
                            <div role="tabpanel" className="tab-pane " id="tab_content2" aria-labelledby="address-tab">
                            <Formik 
                                initialValues={{ file: null }} 
                                validationSchema={Yup.object().shape({
                                    file: Yup.mixed().required()
                                })}
                                onSubmit={(values) => {
                                    const data = new FormData() 
                                    data.append('category', values.file);
                                    data.append('_id', props.modalData._id);
                                    axios({
                                        url : "upload/category",
                                        method : 'POST',
                                        data : data,
                                    }).then( result => {
                                        if(result.status === 200){
                                            props.updateImage({
                                                _id:props.modalData._id,
                                                imageURL: result.data.data.imageURL,
                                            });
                                        }
                                    }).catch( e => {

                                    });

                                }}
                            > 
                                {({ values , errors, handleSubmit, setFieldValue }) => (
                                    <Form onSubmit={handleSubmit} encType="multipart/form-data">
                                        <div className="form-group">
                                            <label htmlFor="file">File upload</label>
                                            <Field id="file" name="file" type="file" value="" title="qwe" className="form-control" onChange={(event) => { 
                                                setFieldValue("file", event.currentTarget.files[0]);
                                            }} />
                                            {errors.file ?
                                              <div>{errors.file}</div> 
                                            : null}
                                            {values.file ? 
                                                (<span>{values.file.name}</span>)
                                            : null}
                                            {/* <Thumb file={values.file} /> */}
                                        </div>
                                        <button type="submit" className="btn btn-success btn-sm pull-right" > Submit </button>
                                    </Form>
                                )}
                            </Formik>
                            </div>
                        </div>               
                    </div>
                </div>
            </div>
        </div>
    )
}



// class Thumb extends React.Component {
//     state = {
//       loading: false,
//       thumb: undefined,
//     };
  

//     componentWillReceiveProps(nextProps) {
//         if (!nextProps.file) { return; }
     
//         this.setState({ loading: true }, () => {
//         let reader = new FileReader();

//         reader.onloadend = () => {
//             this.setState({ loading: false, thumb: reader.result });
//         };

//         reader.readAsDataURL(nextProps.file);
//         });
//     }
  
//     render() {
//       const { file } = this.props;
//       const { loading, thumb } = this.state;
  
//       if (!file) { return null; }
  
//       if (loading) { return <p>loading...</p>; }
  
//       return (<img src={thumb}
//         alt={file.name}
//         className="img-thumbnail mt-2"
//         height={200}
//         width={200} />);
//     }
//   }
