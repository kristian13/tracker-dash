import React from 'react';

export const CategoryList = (props) => {
    return (
        <tr>
            {/* <th scope="row">{props._id}</th> */}
            <td>
                <img alt="#" src={props.imageURL} className="img-responsive" width="125"/>
            </td>           
            <td>
                {props.description}
            </td>
            <td>
               {props.isActive ? (<span className="label label-success">Active</span>) : (<span className="label label-danger">In-active</span>)}
            </td>
            <td>
                <button onClick={props.edit.bind(this)} action="edit" className="buttonPrevious btn btn-xs btn-primary">Edit</button>  
                {/* <button onClick={props.delete}>Delete</button> */}
            </td>
        </tr>
    )
}

