import { createStore ,compose, combineReducers, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';


import Auth from './reducers/auth';
import Categories from './reducers/categories';
import Restaurants from './reducers/restaurants';
import Foods from './reducers/foods';
import Menus from './reducers/menus';
import Customers from './reducers/customers';
import Drivers from './reducers/drivers';
import Validations from './reducers/validations';

//LIST OF REDUCERS
const rootReducer = combineReducers({
	categories: Categories,
	restaurants: Restaurants,
	customers: Customers,
	foods: Foods,
	menus: Menus,
	drivers: Drivers,
	auth: Auth,
	validations: Validations
});



// MIDDLEWARE
const logger = store => {
	return next => {
		return action => {
			//console.log('[Middleware] Dispatching ', action);
			const result = next(action);
			//console.log('[Middleware] next state ', store.getState());
			return result;
		}
	}
}

const composeEnhancer = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
export const store = createStore(rootReducer,composeEnhancer(applyMiddleware(logger,thunk)));
