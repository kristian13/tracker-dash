import * as actionTypes from '../actions/actions';

const initialState = {
    token: []
};

const reducer = (state = initialState, action) => {
	switch(action.type){
		case actionTypes.VALID_TOKEN:
			return {
				...state,
					
			};

		case actionTypes.VALID_TOKEN_UPDATE:
			return {
				...state,
				token: [action.payload] 
            }
        default:

	}
	return state;
}

export default reducer;