import * as actionTypes from '../actions/actions';

const initialState = {
	restaurants:[],
	pagination: {
			currentPage:0,
			currentLimit:5,
			recordCount:0
	},
	restoTags: []
};

const reducer = (state = initialState, action) => {
	switch(action.type){
		case actionTypes.RESTAURANT_INIT:
			return {
				...state,
				restaurants: [...action.payload.restaurants],
				pagination:{...action.payload.pagination}
			};

		case actionTypes.RESTAURANT_PAGE_UPDATE:
			return {
				...state,
				restaurants: [...action.payload.restaurants],
				pagination: {...action.payload.pagination}
			}
		
		case actionTypes.RESTAURANT_ADD:
			return {
				...state,
				restaurants: [...action.payload],
			}
		
		case actionTypes.RESTAURANT_EDIT:
			return {
				...state,
				restaurants: [...action.payload],
			}

		case actionTypes.RESTAURANT_EDIT_IMAGE:
		
			let { restaurants } = state;

			let update = restaurants.map(item => {
				if(item._id === action.payload._id){
					return {...item,imageURL:action.payload.imageURL};
				}
				return item;
			});

			return {
				...state,
				restaurants: [...update],
			}
	
		case actionTypes.RESTAURANT_TAGS:
			return {
				...state,
				restoTags:[...action.payload]
			}

		
        default:

	}
	return state;
}

export default reducer;