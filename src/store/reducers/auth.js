import * as actionTypes from '../actions/actions';

const initialState = {
    is_login : false,
    data : {}
};

const reducer = (state = initialState, action) => {
	switch(action.type){
		case actionTypes.AUTH_LOGIN:
			return {
                ...state,
                is_login : true,
                data : action.payload
			};

		case actionTypes.AUTH_LOGOUT:
            localStorage.removeItem('user_data');
			return {
                ...state,
                is_login: false,
                data : {}
            }
        default:

	}
	return state;
}

export default reducer;