import * as actionTypes from '../actions/actions';

const initialState = {
	menus : [],
	pagination : {}
};

const reducer = (state = initialState, action) => {
	switch(action.type){
		case actionTypes.MENU_PAGE_INIT:
			return {
				...state,
				menus:[...action.payload.menus],
				pagination: {...action.payload.pagination}
			};

		case actionTypes.MENU_UPDATE:
			let { menus } = state;
			let update = menus.map( item => {
				if(item._id === action.payload._id) { return action.payload }
				return item;
			});

			return {
				...state,
				menus:[...update]
			};

        default:
			
	}
	return state;
}

export default reducer;