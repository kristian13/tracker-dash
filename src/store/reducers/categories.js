import * as actionTypes from '../actions/actions';

const initialState = {
	categories:[],
	pagination:{
		currentPage:0,
		currentLimit:5,
		recordCount:0
	}
};

const reducer = (state = initialState, action) => {
	switch(action.type){
		case actionTypes.CATEGORY_INIT :
			return {
				...state,
				categories:[...action.payload.categories],
				pagination:{...action.payload.pagination}
			};
			
		case actionTypes.CATEGORY_PAGE_UPDATE :
			return {
				...state,
				categories:[...action.payload.categories],
				pagination:{...action.payload.pagination}
			};

		case actionTypes.CATEGORY_STATE_UPDATE :
			console.log(action.payload);
			return {
				...state,
				categories:[...action.payload]
			};

		case actionTypes.CATEGORY_UPDATE:
			return {
				...state,
			}
			
        default:

	}
	return state;
}

export default reducer;