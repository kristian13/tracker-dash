import * as actionTypes from '../actions/actions';

const initialState = {
	customers : [],
	pagination : {
		currentPage:0,
		currentLimit:5,
		recordCount:0
	}
};

const reducer = (state = initialState, action) => {
	switch(action.type){
		case actionTypes.CUSTOMER_INIT:
			return {
				...state,
			};

		case actionTypes.CUSTOMER_UPDATE:
			return {
				...state,
			};

		case actionTypes.CUSTOMER_PAGE_UPDATE:
			return {
				...state,
				customers:[...action.payload.customers],
				pagination: {...action.payload.pagination}
			};

        default:
			
	}
	return state;
}

export default reducer;