import * as actionTypes from '../actions/actions';

const initialState = {
	foods : [],
	pagination : {
		currentPage:0,
		currentLimit:5,
		recordCount:0
	}
};

const reducer = (state = initialState, action) => {
	switch(action.type){
		case actionTypes.FOOD_PAGE_INIT:
			return {
				...state,
				foods:[...action.payload.foods],
				pagination:{...action.payload.pagination}
			};

		case actionTypes.FOOD_UPDATE:
			return {
				...state,
			};

		case actionTypes.FOOD_PAGE_UPDATE:
			return {
				...state,
				menus:[...action.payload.menus],
				pagination: {...action.payload.pagination}
			};

        default:
			
	}
	return state;
}

export default reducer;