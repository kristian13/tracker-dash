import React from 'react';
import {connect} from 'react-redux';
import Dashboard from './theme/dashboard';
import LoginPage from './theme/loginpage';


class Authpage extends React.Component {

    constructor(props){
        super();

    }

    componentDidMount(){
        if(localStorage.user_data){
            let data = JSON.parse(localStorage.getItem('user_data'));
            this.props.setAuth(data);
        }
    }
    
    render () {
      
        let page =  this.props.state.auth.is_login === true ? (<Dashboard/>) : (<LoginPage/>) ;  
    
        return (
            <div>
              {page}
            </div>
        )
    }
}

const mapStateToProps = state =>{
    return {
        state: state
    }
  }
  
const mapDispatchToProps = dispatch => {
    return {
        setAuth:(data)=> dispatch({type:'AUTH_LOGIN',payload:data}),
    }
}

export default connect(mapStateToProps,mapDispatchToProps)(Authpage);