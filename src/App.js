import React from 'react';
import './App.css';

import { BrowserRouter as Router } from "react-router-dom";
import Authpage from './Authpage';

//REDUX
import {store} from './store/store';
import { Provider } from 'react-redux';

function App() {
	return (
		<Provider store={store}>
		<Router>
			<div className="App">
				<Authpage/>
			</div>
		</Router>
		</Provider>
	);
}



export default App;
